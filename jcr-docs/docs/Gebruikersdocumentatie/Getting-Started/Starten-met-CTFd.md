<!--
title: Starten met CTFd (lokaal)
description: 
published: true
date: 2022-11-14T15:39:48.900Z
tags: 
editor: ckeditor
dateCreated: 2021-12-21T14:00:46.058Z
-->

# Starten met CTFd lokaal
Gebruik de GitLab repository [Getting-started](https://gitlab.com/jointcyberrange.nl/getting-started) om te beginnen met de Joint Cyber Range applicatie op uw eigen laptop te draaien.

Mogelijk zijn [deze workflows](https://gitlab.com/jointcyberrange.nl/ctfd-docker-with-plugins#most-basic-workflow) handiger.

U kunt vervolgens statische (file- or text-based) en container-based CTF challenges ontwikkelen en uitvoeren.

- [Backup maken van het CTF event](https://gitlab.com/jointcyberrange.nl/getting-started#16-backup-and-restore)

## Development
Wilt u meehelpen met de ontwikkeling van het platform lees dan verder.

In de volgende GitLab repository wordt de CTFd code van de Joint Cyber Range gehost, naast de CTFd core bevinden zich hier de door JCR ontwikkelde CTFd plugins.

- [JCR CTFd repo](https://gitlab.com/jointcyberrange.nl/ctfd-jcr/-/tree/main/CTFd/plugins)
- [JCR CTFd plugins](https://gitlab.com/jointcyberrange.nl/ctf-platform/-/tree/master/CTFd/plugins)
