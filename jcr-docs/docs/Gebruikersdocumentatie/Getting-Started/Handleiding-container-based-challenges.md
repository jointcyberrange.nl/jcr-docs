---
title: Handleiding container based challenges
description: 
published: true
date: 2022-01-14T13:11:07.889Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:00:41.698Z
---

<!--
title: Handleiding container based challenges
description: 
published: true
date: 2022-01-14T13:00:21.960Z
tags: 
editor: undefined
dateCreated: 2022-01-14T13:00:21.960Z
-->

# Handleiding container based challenges 

## Requirements

Om een challenge te bouwen zijn de volgende requirements:

- Docker | [Docker Desktop for Mac and Windows | Docker](https://www.docker.com/products/docker-desktop) | [Welcome! | minikube (k8s.io)](https://minikube.sigs.k8s.io/docs/)

- Kubernetes | [What is Kubernetes? | Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/) | [Getting Started with Kubernetes on Docker Desktop (play-with-docker.com)](https://birthday.play-with-docker.com/kubernetes-docker-desktop/)

- Kompose | [Kubernetes + Compose = Kompose](https://kompose.io/)

## Docker-compose

### Eisen

- Geen persistent volumes | [Persistent Volumes | Kubernetes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

- Secrets in environment variabelen | [Define Environment Variables for a Container | Kubernetes](https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/)

- Documentatie over de uitrol van de omgeving  
Wat vereist jouw gemaakte omgeving.

- Documentatie over het gebruik van de omgeving  
Waar worden vlaggen geplaatst. Wat moet worden gedaan op de omgeving.

### Persistentie van volumes bereiken

Maak een Docker image waar alle bestanden op de juiste plaats staan en zet vervolgens deze image op een docker registry (het voorbeeld maakt gebruik van docker hub).

Stap 1 is het maken van een Dockerfile. Het onderstaande is een voorbeeld.

```plaintext
FROM wordpress
COPY . /var/www/html
```

> :warning:Let op! Bij ontwikkelen op Windows zijn bestanden gemaakt in Windows met line-ending CRLF. Bestanden die gekopieërd worden naar de container moeten omgezet worden naar line ending format voor Linux, dat is LF.

Het bouwen en pushen van een Docker image is te doen met de volgende commando’s (`mvdb010` is een user op `dockerhub.com`).

```plaintext
docker build -t mvdb0110/mkb-wordpress:latest .
docker push mvdb0110/mkb-wordpress:latest
```

Om deze image lokaal te laten draaien als container, gebruik het volgende commando. De optie `-d` staat voor daemon of detached mode, en de optie `-p` zorgt voor de portmapping.

`docker run -d -p 8080:80 mvdb0110/mkb-wordpress:latest`

Hierna kan de wordpress server worden bereikt via `http://localhost:8080`.

Voor meer informatie over Docker images wordt verwezen naar [Building Docker Images with Dockerfiles - Codefresh](https://codefresh.io/docker-tutorial/build-docker-image-dockerfiles/).

### Voorbeeld docker-compose

Docker-compose maakt het mogelijk meerdere services, met een intern netwerk, tegelijk aan te maken. Een werkend voorbeeld van docker-compose is hieronder te vinden. Maak de file `docker-compose.yml` met deze inhoud.

```plaintext
version: '3.3'

services:
   db:
     image: mysql:5.7
     ports:
       - "3306:3306"
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: this_is_very_secure
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: this_is_very_secure

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "81:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: this_is_very_secure
       WORDPRESS_DB_NAME: wordpress
```

Vervolgens kunnen met het commando

```plaintext
docker-compose up
```

deze services worden opgestart. De WordPress server is nu bereikbaar onder `http://localhost:81`.

Een uitgebreidere versie:

```plaintext
version: "3.2"

services:

    mariadb:
        # De image verwijst naar de aangemaakte docker image in een publieke docker registry
        image: mvdb0110/mkb-mariadb:latest
        # De container_name is belangrijk voor het verbinden met andere componenten.
        # Deze naam wordt namelijk straks de DNS naam.
        container_name: mariadb
        # Deze zijn extreem belangrijk, want dit maakt de daadwerkelijke service aan, zodat de componenten elkaar kunnen bereiken.
        ports: 
            - 3306:3306
        # Dit zijn de variabelen die aan de image mee worden gegeven bij deployment.
        environment: 
            - MYSQL_DATABASE=mariadb
            - MYSQL_USER=mariadb
            - MYSQL_PASSWORD=mariadb
            - MYSQL_ROOT_PASSWORD=321verysecurerootpassword123

    wordpress: 
        depends_on: 
            - mariadb
        image: mvdb0110/mkb-wordpress:latest
        container_name: wordpress
        links:
            - 'mariadb:mysql'
        ports: 
            - 80:80
        environment: 
            - WORDPRESS_DB_NAME=wordpress
            - WORDPRESS_DB_USER=root
            - WORDPRESS_DB_HOST=mariadb
            - WORDPRESS_DB_PASSWORD=321verysecurerootpassword123
            - WORDPRESS_CONFIG_EXTRA=define( 'WP_HOME', 'http://localhost' );define( 'WP_SITEURL', 'http://localhost' );
    firefly: 
        depends_on: 
            - mariadb
        image: jc5x/firefly-iii:latest
        container_name: firefly
        ports: 
            - 80:8080
        environment: 
            - APP_KEY=dRZ1lRipw1htgeanQrmjRcD3PGGJjvlk
            - DB_CONNECTION=mysql
            - DB_DATABASE=firefly
            - DB_USERNAME=root
            - DB_PASSWORD=321verysecurerootpassword123
            - DB_HOST=mariadb
            - DB_PORT=3306

    orangehrm:
        depends_on: 
            - mariadb
        image: mvdb0110/orangehrm:latest
        container_name: orangehrm
        ports: 
            - 80:8080
        environment: 
            - ORANGEHRM_DATABASE_NAME=orangehrm
            - ORANGEHRM_DATABASE_USER=root
            - ORANGEHRM_DATABASE_PASSWORD=321verysecurerootpassword123
            - ORANGEHRM_USERNAME=orangehrmuser
            - ORANGEHRM_PASSWORD=orangehrmpassword
    
    backup:
        depends_on:
            - mariadb
        container_name: backup
        image: mvdb0110/mkb-backups:latest
        links:
            - mariadb
        environment:
            - DB_SERVER=mariadb
            - DB_NAME=mariadb
            - DB_USER=root
            - DB_PASSWORD=321verysecurerootpassword123
            - DB_DUMP_FREQ=1440
            - DB_DUMP_BEGIN=1410
            - MD5=FALSE
            - COMPRESSION=GZ
            - SPLIT_DB=TRUE
```

### Uitvoeren van docker-compose in Kubernetes

Wanneer de challenge gemaakt is in docker-compose vragen wij om deze om te zetten naar Kubernetes. Dit kan bij een volgens richtlijnen opgestelde docker-compose.yml met de tool kompose. [User Guide (kompose.io)](https://kompose.io/user-guide/)

|     | **Stap** | **File** | **Input** |
| --- | --- | --- | --- |
| 1   | Open Terminal | n.v.t. |     |
| 2   | Maak map en verander je werkmap | `~\Desktop\kompose` | mkdir ~\\Desktop\\kompose cd ~\\Desktop\\kompose |
| 3   | Plaats valide docker-compose bestand in de map | docker-compose.yml | mv \\path\\to\\docker-compose.yml ~\\Desktop\\kompose\\ |
| 4   | Converteer docker-compose met de tool kompose. | docker-compose.yml | kompose convert |
| 5   | Test de Kubernetes files. | Output vorige commando | kubectl apply -f . -n namespace |
| 6   | Kijk welke services inmiddels zijn opgestart |     | kubectl get services |
| 7   | Open een lokale poort voor verkeer van een aangemaakte Kubernetes service. | n.v.t. | kubectl port-forward service/servicename local:remote -n namespace |
| 8   | Open verkeer voor de service op de lokale poort. | n.v.t. |     |