---
title: Player FAQ
description: 
published: true
date: 2022-11-15T14:04:38.990Z
tags: 
editor: markdown
dateCreated: 2022-01-14T14:02:05.431Z
---

# Player FAQ
- How to learn and play?
	- Start with the [arcade](https://arcade.jointcyberrange.nl) demo environment. Free registration.
  
- Hoe start ik lokaal een CTFD omgeving op mijn eigen laptop?
	- Gebruik de GitLab repository [Getting-started](Starten-met-CTFd.md)
  
---
- Wat gebeurt er met mijn gebruikersdata?
	- Er worden een minimaal aantal persoonsgegevens verzameld en opgeslagen in een beveiligde database (per tenant).
  - Acties die je uitvoert op het platform worden gelogd, met de doelstelling het platform en onderwijsmateriaal te verbeteren.

---

- Waar kan ik meer leren over cyber security?
	- Luister naar de [podcast TS101](https://open.spotify.com/show/1wt6F8iyQWGLPQZE1l90Qt) van Nederlandse bodem voor de grondbeginselen over cyber security 
	- [Darknet Diaries](https://open.spotify.com/show/4XPl3uEEL9hvqMkoZrzbx5) is een andere podcast en gaat in echte gebeurtenissen in de wereld van cyber security en hacking.
  - Wil je zaken in de praktijk brengen? Daar kan het YouTube channel [HackerSploit](https://www.youtube.com/c/HackerSploit) mee helpen.

---

- Waar kan ik met vragen terecht (vragen, feedback, bugs, security issues, etc.)?
	- Het eerste contactpunt voor players/studenten is de beheerder van jou CTF omgeving.
  - Als deze je niet verder kan helpen is het mogelijk contact op te nemen d.m.v. mail. 

---

- Hoe verander ik mijn wachtwoord?

---

