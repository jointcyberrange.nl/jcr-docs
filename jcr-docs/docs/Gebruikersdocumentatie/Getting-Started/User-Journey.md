---
title: User Journeys
description: 
published: true
date: 2022-02-01T08:07:46.893Z
tags: 
editor: markdown
dateCreated: 2022-02-01T07:56:51.883Z
---

# User Journey's
Dit is een user journey voor developers die de getting started willen doorlopen van de JCR. In deze user journey wordt visueel weergegeven welke stappen als eerst genomen moeten worden om een goede start te maken in het project. Zie link naar het volledige document.

https://hogeschoolutrecht.sharepoint.com/:w:/r/sites/Eerstemeeting-Project180-HU-InstituteforICT/Gedeelde%20documenten/Project%20INNO%20258/Stefan/User%20Journeys%20getting%20started.docx?d=w46e07e9064ac43188fd5bd664dc713b8&csf=1&web=1&e=1nh9V3

![user_journey_dev.jpg](user_journey_dev.jpg)

![user_journey_teammember.jpg](user_journey_teammember.jpg)
