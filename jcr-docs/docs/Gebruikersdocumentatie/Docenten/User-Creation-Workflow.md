---
title: User Creation Workflow
description: 
published: true
date: 2022-01-19T15:13:00.392Z
tags: docenten
editor: markdown
dateCreated: 2021-12-21T14:00:33.306Z
---

# User creation workflow 

De Joint Cyber Range kent momenteel drie bronnen vanwaar users aangemaakt kunnen worden.

- **SURFconext (OpenID Connect)** \- de user is aangemaakt en beschikbaar voor applicaties van de aangesloten onderwijsinstelling (dus inclusief de Joint Cyber Range).

	- Een aansluit overeenkomst is vereist in een productie omgeving, een test omgeving is beschikbaar op aanvraag bij SURF.

	- OpenID Connect wordt gebruikt, hierdoor is het mogelijk om bijv. ook Microsoft, Goolge, GitHub of andere op Oath & OpenID Connect gebasseerde identity providers te gebruiken.

- **CSV import** \- users kunnen in bulk worden geïmporteerd d.m.v. van een CSV-bestand.

- **Handmatig registreren van accounts** \- users kunnen zelf een account registreren, opties;

	- Registratie code vereist bij het aanmelden op een CTFd instance.

	- Handmatige user verificatie door een admin.

	- Email verificatie d.m.v. een SMTP relay (Outlook, Gmail, etc).

![](Assets/account-creation-diagram.png)

# CSV import guide

CTFd biedt de mogelijkheid om users te importeren d.m.v. een CSV-bestand. Hiervoor is een template opgesteld middels Excel, deze kan aangepast worden en vervolgens worden omgezet naar het CSV formaat wat CTFd verwacht (CSV met komma als scheidingsteken) .

Download de users template.

[Template users.xlsx](Assets/template_users.xlsx)

Wijzig het domein naar wat de school in gebruik heeft in het tweede werkblad (Opties).

![](Assets/domein.png)

Wijzig de template met de gegevens voor alle gewenste users; name, email en (default) password.

![](Assets/template.png)

Voor het omzetten van CSV van en naar XLSX kunt u gebruik maken van het volgende [Python script](Assets/csv-xlsx-conversion.py). Omgezette bestanden worden in de map van het script geplaatst.

```Bash
python3 csv-xlsx-conversion.py
[?] What conversion do you want to make?: From Excel to CSV
   From CSV to Excel
 > From Excel to CSV
[?] Where is your XLSX input file located?: /mnt/f/Downloads/template_users.xlsx
```

U kunt het omzetten ook handmatig doen.
Sla het bestand op met de bestandsextensie `.csv`.

![](Assets/csv.png)

CTFd verwacht dat het scheidingsteken wat wordt gebruikt in het `.csv` bestand, een komma `,` is. Open het opgeslagen `.csv` bestand met een text-editor, zoals NotePad++ of Visual Studio Code. Vind de find and replace functie (`CTRL+H`). Find what: `;` & Replace with: `,`

Dit resulteert in het volgende.

![](Assets/resultaat.png)

Login als admin op de CTFd instance, klik op `Admin Panel` en vervolgens op `Config`. Selecteer nu de optie `Backup` en vervolgens `Import CSV`. Zorg ervoor dat het `CSV Type` op `Users` staat. Klik nu op `Browse` en selecteer jou gemaakte CSV-bestand met de juiste users.

![](Assets/users.png)

Controleer of de users zijn aangemaakt in de Users tab in de menu balk.

![](Assets/controle-users.png)