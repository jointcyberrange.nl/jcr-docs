---
title: Docenten FAQ
description: 
published: true
date: 2022-11-14T15:41:55.302Z
tags: 
editor: markdown
dateCreated: 2022-01-14T13:54:45.359Z
---

# Docenten FAQ

- Hoe start ik een CTFd omgeving op mijn eigen laptop?
	-	Gebruik de GitLab repository [Getting-started](https://gitlab.com/jointcyberrange.nl/getting-started.md)

---

- Hoe vraag ik een publieke omgeving aan bij de platformbeheerder?
  - Zie de werkinstructie: [Tenant instance aanvragen](Tenant-instance-aanvragen.html)

---

- Hoe registreer/importeer ik gebruikers naar CTFd
	- Zie de [CSV import guide](User-Creation-Workflow.md#csv-import-guide)

---

- Hoe voeg ik CTFd challenges toe?

---

- Hoe kan ik een back-up maken van het CTFd event en terugzetten (backup en restore)?
	- Volg de instructies uit [getting started](https://gitlab.com/jointcyberrange.nl/getting-started#16-backup-and-restore)
  - Let op: onthoud bij een backup de usernaam en wachtwoord van een admin. Alle users en admins worden namelijk ook teruggezet.

---

- Welke applicatie opties/Environment variables zijn er aanwezig?

---

- Wat zijn de algemene voorwaarden van het platform?

---

- Wat gebeurt er met gebruikersdata?
	- Lees onze [privacy policy](https://jointcyberrange-nl-privacy.carrd.co/)

---

- Waar kan ik met vragen terecht?
	- Gebruik issues in gitlab.
