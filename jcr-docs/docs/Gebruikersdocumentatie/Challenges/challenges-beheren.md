---
title: Challenges beheren
description: Dit is een gebruikershandleiding voor de challenge manager website.
published: true
date: 2022-11-11T12:56:53.867Z
tags: ctf challenges
editor: markdown
dateCreated: 2022-07-04T19:20:46.401Z
---

# Challenge Manager User Manual
This webapp has been built for teachers who use the CTF challenges provided by Joint Cyber Range.

Obsolete

1. Go to https://challenge-manager-jcr.herokuapp.com/
2.	Click the “Import” button.
![import.png](../challenge-beheer/assets/import.png)
3.	Choose the desired zipfile representing a CTFd export.
	3.1 Click “Open”.
4.	The CTF challenges are now able to be viewed or selected.
 	4.1 Decide if you would like to import more CTFd exports.
 		&emsp;4.1.1 Would you like to add another CTFd export? Repeat from step 2.
 		&emsp;4.1.2 Would you like to overwrite the previously uploaded CTFd export? Repeat from step 2 with the “Overwrite previous challenges” checkbox checked:
    ![overwrite.png](../challenge-beheer/assets/overwrite.png)

5.	If you would like to filter through the list of CTF challenges, click on the “Search” bar and type in any variables you would like to filter on:
![search.png](../challenge-beheer/assets/search.png)
	5.1 Would you like to just view challenges? Proceed to step 6.
	5.2 Would you like to update the list of CTF challenges in your CTFd export? Proceed to step 7.
6.	Click on a CTF challenge to view its information.
![info.png](../challenge-beheer/assets/info.png)
7.	Select the desired CTF challenges.
![examplechallenge.png](../challenge-beheer/assets/examplechallenge.png)
8.	Choose if you allow your personal data that is stored inside the CTFd export to be exported. This clears multiple tables containing personal data like users, teams, submissions, etc.
 	8.1 Yes? Proceed to step 9.
 	8.2 No? Check the checkbox “Remove personal data” and proceed to step 9.
  ![personaldata.png](../challenge-beheer/assets/personaldata.png)
9.	Click the “Export” button to download your updated CTFd export with an updated list of challenges.
 ![export.png](../challenge-beheer/assets/export.png)
10.	Are you done and would you to clear the screen and currently loaded exports? Click the button “Clear Session”.
 ![clearsession.png](../challenge-beheer/assets/clearsession.png)
 
When choosing to remove personal data (see user manual step 8), a standard user will be added with the following credentials:
- Username: `admin`
- Email: `admin@mail.com`
- Password: `admin`

 This project is hosted at https://gitlab.com/jointcyberrange.nl/challenge-manager.
