---
title: Challenges FAQ
description: 
published: true
date: 2023-04-13T14:37:24.425Z
tags: 
editor: markdown
dateCreated: 2022-01-14T13:45:16.913Z
---

# Challenges FAQ

CTF challenges basically run down to; IT/Cyber Security puzzles, a digital scavenger hunt or escape room. Various types of CTF events exist, while CTFd makes it possible to host Jeopardy Style events.
Events focussed on professionals or internal training, try to simulate realistic Cyber Security scenarios. Attack/Defence games or Red vs Blue scenarios, is where the Joint Cyber Range will be heading to in the future.

## Before making a challenge

- Make sure to have a working local CTFd instance, see [Starten met CTFd lokaal](../Getting-Started/Starten-met-CTFd.md). For a simple vanilla CTFd instance use this [Docker image](https://hub.docker.com/r/ctfd/ctfd) (without our container challenges feature).
- General CTFd [tutorials](https://docs.ctfd.io/tutorials).

## CTFd Challenges

- What about some example challenges?
  - [JuiceShop](https://gitlab.com/jointcyberrange.nl/ctfd-public-container-challenges/juiceshop-for-jcr) with OWASP vulnerabilities.

- How do I make my own CTF challenge?
  - Workflow standard challenge (see [tutorials](https://docs.ctfd.io/tutorials))
  - Workflow [container based challenges](container-challenges-workflow.md)
  - Workflow dynamic challenge

## Backup

- Is it possible to make a backup of my challenges?
  - You can create a backup by exporting a zip file from your local CTFd instance, follow the steps at [Backup and restore](https://gitlab.com/jointcyberrange.nl/getting-started/-/tree/main#16-backup-and-restore).

## Managing

- How do I manage challenges
  - Manage challenges inside CTFd
  - Use the [Challenge Manager](challenges-beheren.md)

## Categories

- What type of CTF challenges and categories are there?
	- Types of [CTF challenges and categories](Soorten-CTF-challenges-en-categorieen.html)

---

- What are the challenge requirements?
  - Get your challenges peer reviewed