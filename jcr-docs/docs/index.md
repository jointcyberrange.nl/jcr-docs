# Joint Cyber Range

Welkom bij de documentatie van de [Joint Cyber Range](https://www.jointcyberrange.nl). De nationale faciliteit voor cyber security onderwijs in Nederland, ontwikkeld door en voor het hoger- en beroepsonderwijs.

![jointcyberrange.png](jointcyberrange.png)

## Getting-Started

- [Starten met CTFd *JCR lokaal, een omgeving op je eigen laptop.*](Gebruikersdocumentatie/Getting-Started/Starten-met-CTFd.md)

## Player (student)

- [Player FAQ *Meedoen aan een cursus, wedstrijd of open event als player/student.*](Gebruikersdocumentatie/Getting-Started/Player-FAQ.md) 

## Leader (docent / event admin)

- [Leader FAQ *Beheer een CTF event voor de cursus, wedstrijd of een open event.*](Gebruikersdocumentatie/Docenten/Docenten-FAQ.md)

## Challenge Developer

- [Challenges FAQ *Ontwikkeling van CTF challenges die als lesmateriaal kunnen dienen.*](Gebruikersdocumentatie/Challenges/Challenges-FAQ.md)

## Platform Developer

- [Developer FAQ *Ontwikkeling en onderhouden van Joint Cyber Range (JCR) code, services & cloud infra.*](Development/Developer-on-boarding/FAQ-voor-developers.md) 

## Admin (platform beheerder)

- [Admin FAQ *Beheerder van het algehele platform en het contact punt voor leaders en overige stakeholders.*](Beheer/Admin-FAQ.md)

## Organisatie (mensen, scholen, partners)

- [Organisatie FAQ *Doelen,opzet en besturing van de JointCyberRange als organisatie.*](Beheer/Organisatie-FAQ.md)

## Eigenschappen

- [Features *CTFd core met alle daarop door studenten ontwikkelde uitbreidingen d.m.v. plugins en/of services.*](Development/Architectuur-&-Diagrammen/Features.md)
- [Statische file-based challenges *Standaard challenge type uit CTFd core*](Gebruikersdocumentatie/Challenges/Statisch-file-based.md)
- [Container-based challenges *JCR custom CTF challenge type*](Gebruikersdocumentatie/Challenges/container-based.md)

## Contributing guidelines

[Contributing-Guidelines *Iedereen mag helpen dit platform te verbeteren. Graag zelfs en alle input is welkom!. Don't be shy.*](Development/Contributing-Guidelines.md)

[Link to Gitlab repository for this documentation](https://gitlab.com/jointcyberrange.nl/jcr-docs/). Suggest improvements by issuing a Merge Request, or filing an issue.

On the wishlist: git link on every page (theme), some html conversion (ChatGPT is your friend for that), general correctness checks.