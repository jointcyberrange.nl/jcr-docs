---
title: Handleiding Selenium Browser IDE
description: 
published: true
date: 2022-01-31T11:21:25.982Z
tags: selenium, browser, ide
editor: markdown
dateCreated: 2022-01-31T09:58:10.427Z
---

# Handleiding Selenium Browser IDE
Stap 1: Open Google Chrome Browser

Stap 2: Ga naar de Chrome Webstore

Stap 3: Zoek en Installeer de extensie Selenium IDE

https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd?hl=nl

Stap 4: Klik op het puzzelstukje (extensies), naast de URL bar in Chrome

Stap 5: Klik op de extensie Selenium IDE om op te starten

Stap 6: Klik ‘Create a new project’

Stap 7: Geef projectnaam op

Stap 8: Klik linksboven op het ‘+’ teken in de balk ‘tests’, om een nieuwe testcase aan te maken

Stap 9: Voeg ‘playback base URL’ toe (Website die je wilt testen). Begin met https:// (vervolgens type URL zonder www.)

Stap 10: Klik rechtsboven op de rode ‘rec’ button om de betreffende pagina (automatisch) te openen en het testen te starten.

Stap 11: Voer gewenste test uit

Stap 13:  Klaar met de test? Ga terug naar Selenium en druk rechtsboven eerst op de rode button ‘stop recording’. De test is nu geregistreerd

Stap 14: Klik linksboven op de ‘play’ button om gemaakte test automatisch te herhalen

Stap 15: Ga terug naar Selenium en klik op linksboven op ‘stop’

Stap 16: Sla je project op door rechtsboven op ‘Save’ te klikken, kies een map om in op te slaan.
