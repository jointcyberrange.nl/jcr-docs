---
title: Remove leaked secrets in Git repository history
description: 
published: true
date: 2022-01-14T08:38:34.012Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:01:25.499Z
---

# Remove leaked secrets in Git repository history 

Oops, I have leaked a secret in a Git repository.  
I deleted the file or text, but the original continues to persist in Git history.

# ToDO's

-   How to back-up and restore a repository in its entirety?
-   What to do when working with multiple people?
-   How to assure the secret is completely gone?

**Warning!**

Be aware of the dangers involving tempering with Git history!  
Also, when working together, it's possible that people can push/merge the secret back into the repository when they already have it locally.

Source: [https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository](https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository)

# Requirements

- Permission to force push to the repository. **(Project) Settings > Repository > Protected branches**

- GitLab Personal Access Token with `Read_repository` and `Write_repository` scopes. **Preferences > Personal Access Tokens**

## Install BFG Repo-Cleaner

Create a file `install_bfg.sh` and copy the contents from below.

```plaintext
#!/bin/bash
set -e

# install java
sudo apt -y install default-jre-headless

# fetch latest bfg.jar
sudo wget https://repo1.maven.org/maven2/com/madgag/bfg/1.13.0/bfg-1.13.0.jar -O /usr/local/bin/bfg-latest.jar

# install to /usr/local/bin
echo -e "#!/bin/bash\njava -jar /usr/local/bin/bfg-latest.jar \$@" | sudo tee /usr/local/bin/bfg

# mark executable
sudo chmod +x /usr/local/bin/bfg
```

Install BFG Repo-Cleaner by calling the Bash script: `./install_bfg.sh`.

## Remove the leaked secret

First you'll have to remove the secret from the repository with a manual action. Delete the relevant file or text containing the secret. Now repair your first initial mistake, by adding the file or whole folder in a `.gitignore` file. While you're at it you can also add it to a `.dockerignore` or other `.ignore` files.

Remove your file with sensitive data and leave your latest commit untouched (you only need to specify the filename, BFG will search the repository!):

`bfg --delete-files YOUR-FILE-WITH-SENSITIVE-DATA`

Alternatively, replace all text listed in the file:

`bfg --replace-text passwords.txt`

Before you continue with the next step and force push the changes to your repository.  
After BFG Repo-Cleaner you can force push the changes to your repository.

`git push --force`

Lastly, don't forget to change the relevant branch back to its previously protected state.