---
title: Roadmap
description: Roadmap for future JCR development
published: true
date: 2022-02-01T08:04:06.514Z
tags: 
editor: markdown
dateCreated: 2022-01-26T13:34:04.949Z
---

# Roadmap

This roadmap is a collection of possible improvements to the platform and is no particular order.

## General

- Introduce a mail system for CTFd, Wiki.js, HubSpot etc.
- Automate on-boarding of new accounts for collaborators, developers and operators by using internal tools. Low-code platforms could help achieve this: AppSmith, BudiBase or ...
- Utilize OpenID Connect for user authentication on all availible apps e.g. Wiki.js, Argo CD, Grafana etc.
- Make adding OpenID connectors part of the general software on-boarding workflow.
- Achieve GitOps wherever possible - read the definition on [OpenGitOps.dev](https://opengitops.dev/).
- Make code more DRY (Don't Repeat Yourself). This can be applied to all forms of code e.g. Python, Kustomize, Markdown or even Excel. Only implementation will differ from various languages/applications: templates/blue prints, overlays or links.

## CTFd (application)

- More scalable file uploads using S3 compatible object-storage.
- Automated MariaDB backups on a schedule.
- Automated MariaDB restore on re-deployment for Disaster Recovery.
- Integrate open source and already existing platforms such as OWASP Juice Shop, Metasploit, Kuberntes Goat, Kypo etc. Try finding a way to add them by applying minimal custom code/config.
- Implement feature flags, opt-in/out features during runtime without re-deployment.
- Simulate real-world scenarios with services such as DNS, BGP, root CA etc.
- Apply a Joint Cyber Range theme to CTFd.
- CTFd modern frontend overhaul using a framework such as React.
- Include fancy and custom UI components with a function.
- Combine CTFd with other frontend applications using a microfrontend framework and overlaying existing frontends apps to become one.
- A shared CTFd tenant with teams for all institutions on the JCR to fight and see who's the best.
- HTTP logging in the console must be related to users and their browser activity / clicks or events.
- Importing and exporting data from/to CSV files must be improved.
- Enable collaborators to donate challenges for the public.
- Multiple container challenge types: web, ssh, ftp, etc.

## CTF challenges

- Automatically delete stale / in-active but still running container challenges.
- Self-hosted container registry (Harbor vs GitLab) on the cluster for faster image pulls and having more privacy / control. Maybe even a registry per tenant?
- Set a maximum time before challenge wil delete and provide a timer in the UI.
- Use a mono repository design for CTF challenges.
- CTF challenges as code in Git with GitOps-like sychronization to CTFd by using the ctfcli or ...
- Isolate container challenges to run only in a separete cluster or vCluster.
- Stopping a container challenge takes quite some time. During this time, users must not be allowed to open the challenge until the stopping is finished.
- The content of the docker-compose textbox in the container challenge creation screen must be checked on formatting.
- The challenge registry must be customizable in the container challenge configuration. Currently all challenges are stationed in the JCR Gitlab.
- A public tenant as showroom. All institutions may show their paticular expertise in a CTF category that represents them and donate challenges to the platform.
- Build a CTF challenges catalogs with categorization, subcategories (multiple levels?), tags and search.
- Kubernetes healthchecks for challenges, could possibly be done using (correct labels](<https://github.com/kubernetes/kompose/blob/master/docs/user-guide.md#labels>).
- Unique flags on static challenges, for example random strings for crypto challenges or generating files.
- Centralised flag generator and injection for container challenges.
- Verify no unwanted vulnerabilities and attacks are possible besides the intented ones for the challenge by applying DevSecOps security scanning. Think of vulnerability scanning, SAST, DAST or even full automated pentesting.

## Platform

- Try to bring all DevOps tools together in one unified software-catalogues and developer self-service platform. e.g. Backstage, clutch.sh or PlatformOps.
- Commit to building internal tooling, lessening the management burden, repatative commands and repeating / manual tasks. Doing this, you also prepare for bringing tools together and later implement ChatOps.

### Kubernetes

- Aim for zero-downtime upgrades of applications or even clusters. Start by using the correct pod update strategy.
- Migrate from RKE to a more modern distribution using containerd or have a configurable container runtime. Think of Kubespray, Compliant Kubernetes or RKE2. Favorable is a distro that's secure by default and takes the management burden away.
- Try to follow best-practices and industy standards when relevant. Check off some boxes that could be easily achieved.
- Introduce healthchecks by utilizing Liveness, Readiness and Startup Probes
- Define resource quotas on the pod and namespace level. Possibly use [goldilocks](https://www.fairwinds.com/blog/introducing-goldilocks-a-tool-for-recommending-resource-requests) for recommending on the limits to set.
- Autoscaling of workloads on the pod and later also on the node level.
- Utilize HashiCorp Vault (or derivatives) for secret management P.S. Mount secrets as close as possible to the target.
- Enable encryption for Kubernetes secrets at rest.
- Encrypt all secrets left-over in Git repositories, that are not compatible with the centralised secret management solution.
- Automated backups using Velero + Restic vs Stash vs Kasten K10 and integrate it within the GitOps workflow.
- Automate the recovery of backups on re-deployments, in a best case scenario you only include data that's not immutable or could come from another data source.
- Understand, document and use RBAC and ServiceAccounts.
- Isolate by using NetworkPolicies.
- Use the ReclaimPolicy Retain on PersistentVolumes to persist data on the host after the application is deleted.
- Introduce Access Controls for accessing the Kubernetes Control-Plane / Kubernetes-API.
- Make dashboards for internal use privatly accessible e.g. Grafana and Argo CD.
- Use kubernetes native concepts such as CustomResources and CustomResourceDefinitions to achieve X / Anything as Code and apply GitOps to that.
- Introduce Chaos Engineering for testing self-healing capabilities. Perform this on various levels: Pod, Deployment, Namespace, Node, Cluster etc.
- Highly available and seperated master, worker and control plane nodes and isolate etcd.
- Use a default static webpage with correct error codes, messages and JCR theming for ingress resources not available.
- Remove old / stale containers using something like kube-janitor or alternative.

### CI/CD

- Only start a pipeline on a change for relevant files/folders. No Kaniko build for example when only Markdown was edited.
- Make the pipeline less manual.
- Make use of unique container tags instead of using the latest tag, or even better use digest hashes.
- Define a workflow from A-to-Z / commit, to deployment by Argo CD and tests that run after that.
- Please don't use kubectl access within the pipeline, rather build a kubernetes cluster in the pipeline using K3D, KIND or K3s.
- Introduce Static Analysis such as Linting, Static application security testing(SAST) and spelling checks for all artifacs in code repositories.
- Introduce Dynamic application security testing(DAST) for artifacts at runtime.

### Argo CD

- Build-out and make use of the Argo CD declarative and self-managed setup.
- Extend with Argo Rollouts for progressive delivery utilizing Canary or Blue/Green deployments.
- Introduce Keptn for the orchestration of automation and tests after deployments.
- Utilize config management plugins for tooling not natively supported by Argo CD.

### Observabillity

- Add Prometheus monitoring to all apps running in the cluster.
- Aim to cover the four golden signals of SRE, USE and RED metrics.
- Add blackbox monitoring in addition to whitebox monitoring of Prometheus.
- Evaluate FluentD / Fluent Bit for the logging pipeline.
- Evalutate OpenDistro for Elasticsearch / OpenSearch as logging storage backend.
- Utilize OpenTelemetry and/or Jaeger for tracing.

### Wiki.js (Documentation)

- More references to existing pages
- Use the sidebar / menu of Wiki.js
- Utilize templates for creating pages
- Define documentation and contributing guidelines
- Define guidelines for use of different languages (English & Dutch)
- Automated Postgresql backups on a schedule
- Automated Postgresql restore on a re-deployment for Disaster Recovery
- Improve the user / group import using GraphQL

### Security

- Add runtime protection e.g. Falco IDS and/or ModSecurity WAF NGINX modules
- Add security in every part of the Software Development Lifecycle: development IDE-plugins, pre-commit, staging, production.
- Environments change over time and thus you need to periodically scan / test production environments on security.
- Policies as Code possibly using OPA gatekeeper.
- Improve the security of the JCR by applying concepts from the learning content / CTF challenges.
