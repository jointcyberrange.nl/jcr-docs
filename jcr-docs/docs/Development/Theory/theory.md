---
title: theory
description: 
published: true
date: 2022-01-31T19:35:09.951Z
tags: 
editor: markdown
dateCreated: 2022-01-31T19:31:43.200Z
---

# 1. Theory

This chapter will describe the theory behind the technology concepts touched upon, within the context of the Joint Cyber Range and building a state-of-the-art SaaS (Software as a Service). These include Kubernetes, local development, developer onboarding, GitOps and DevSecOps.

**Main research question:** Which modern DevOps and Cloud tools, techniques and principles can help the JCR deliver software faster, more reliable and securely, while facilitating CTFd-as-a-Service to customers?

## 1.1. Kubernetes

**Research question:** What is the Kubernetes ecosystem about and what entails a distribution of Kubernetes?

[Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/), often abbreviated as K8s, is an orchestration platform for infrastructure automation. Some describe it as the operating system for the datacenter. It can run various kinds of workloads, but is most often utilized for containerized compute workloads. Although, running [stateful](https://glossary.cncf.io/stateful_apps/) workloads has also become more prevalent and reliable in recent years.
With new technologies Kubernetes can also drive virtualized or serverless workloads. Kubernetes is flexible, but can be equally complex.

### 1.1.1. Components

Kubernetes is highly modular and extensible, it consists out of various components that can used interchangeably, or left out entirely. Although, some components, such as the Kubernetes-API are essential. The components can be built and defined by yourself or within a distribution, like in the Linux ecosystem. Often distributions are based on the upstream (official) Kubernetes and customized with components, such as;

- The container runtime, default is containerd
- Control plane storage, default is etcd

Below are addons listed that are not necessarily required by Kubernetes, but are required for a good user experience. Kubernetes distributions should be opinionated in a certain way, but provide enough flexibility towards meeting business needs specific to an organization. A full featured Kubernetes distribution should facilitate these additional components for its users, as a managed or hosted option would;

- DNS server, most prevalent is CoreDNS
- CNI (Container Network Interface) provider e.g. Cannel
- Ingress controller, most prevalent is NGINX
- Storage provider &amp; CSI (Container Storage Interface) e.g. Longhorn

You also would want to have some insights in your cluster. The official Kubernetes dashboard or other adaptations can help with this. Although, these dashboards don't provide information on applications that run in your Kubernetes cluster. Additional monitoring and logging have to be put in place, or even combined with distributed tracing and other more advanced techniques to deliver full observability.

### 1.1.2. Distributions

Various types of Kubernetes distributions are available; open source, paid and managed. Focused on security, resource consumption, ease of use and installation, providing a complete or exotic feature-set. The choice of Kubernetes distribution will often pull you into an ecosystem of connected tooling. The distribution is often tightly coupled with the mechanism used to provision the cluster and its life cycle management. While modern installers facilitate infrastructure provisioning. Some Kubernetes management platforms also provide a mechanism for provisioning full-featured clusters in an opiniated way. In this case a seed cluster, dedicated to provisioning is often required. The most prevalent Kubernetes distributions, installers and management platforms, open source and commercial alike, are mapped on the [CNCF landscape](https://landscape.cncf.io/card-mode?category=certified-kubernetes-distribution,certified-kubernetes-hosted,certified-kubernetes-installer&grouping=category&license=open-source) (Cloud Native Computing Foundation, n.d.).

### 1.1.3. OpenStack + Kubernetes

OpenStack is another compute platform, but for virtualized workloads. It facilitates the tooling to build a traditional IaaS (Infrastructure as a Service), for your own private or public cloud environment. OpenStack can be facilitated through Kubernetes as its under cloud by running its components as containers. While Kubernetes can also be run on top of OpenStack to facilitate container workloads. In this last implementation technique, it&#39;s possible to integrate OpenStack&#39;s compute, storage and networking components with those of Kubernetes (Acedo et al., 2021).

### 1.1.4. Cloud-Native Computing Foundation (CNCF)

The CNCF is a foundation that hosts and governs various open source projects, built to be run in the cloud. Kubernetes was the first CNCF project Kubernetes to enter the graduation fase (Conway, 2018). Prominent Kubernetes and CNCF affiliated tooling can be found on the CNCF landscape. Projects and companies joining on such a landscape, prove their adoption by the community, provides a certain level of maturity, assurance for its future existence and maintenance. Each fase; sandbox, incubation and graduated, show growth in the aforementioned projects characteristics (Khayretdinov, 2018).
The CNCF itself is governed by the Linux Foundation. The Open Infrastructure Foundation and the Apache Software Foundation are other organizations that host open source projects, but with a different scope.

## 1.2. Development experience (DevEx)

**Research question:** How to achieve a great Development Experience and how does it relate to developer onboarding?

The development experience is essential for an effective workflow. From the first day of the on-boarding process to entirely ingrained into the project. Developers should be provided with  documentation on various levels and the right tooling to get started. Repetitive work should be eliminated with blue prints / templates and should be available on-demand. Self-service development platforms are a recent trend in the DevEx space. Services should be facilitated where needed following shift-left and self-service principles.

Shifting left is the principle of improving [developer effectiveness](https://martinfowler.com/articles/developer-effectiveness.html), by empowering developers to perform tasks themselves. Tasks such as applying configuration management, cloud services or fix issues. Catching issues early in the development life cycle is easier and more economic. Issues can be related to various areas; styling, tests, bugs, security, compliance etc. This shortens the developer [feedback-loop](https://www.whitesourcesoftware.com/resources/blog/shift-left-testing/). Preferably issues are remediated before changes are pushed to central systems. Either with pre-commit hooks or IDE plugins, live or with every save. This eliminates waiting for CI/CD pipelines performing various tests and tasks. 

![https://www.whitesourcesoftware.com/wp-content/media/2021/02/Picture1.png](https://www.whitesourcesoftware.com/wp-content/media/2021/02/Picture1.png)

Developers should worry about their code and less about its underlying infrastructure. Kubernetes is complex, therefore its components should be abstracted towards the use by developers (Gienow, 2021). However the development environment should represent production as much as possible to remediate possible issue's early on in the development cycle. For example, use containers for development when using them in production. The same could be said for Kubernetes.

## 1.3. GitOps

**Research question:** What is GitOps and how does it accelerate software development?

GitOps is often described as "operations by pull requests", in that the desired state of the system will be pulled automatically. With GitOps the desired state is expressed using a declarative approach. This state must be stored within a version control system and act as single source of truth. To prevent configuration drift, the target system will be continuously reconciled, by scanning the system and trying to apply the desired state.

Recently, the [OpenGitOps Project](https://opengitops.dev/) defined four GitOps principles:

- **Declarative:** A system managed by GitOps must have its desired state expressed declaratively.
- **Versioned and Immutable:** Desired state is stored in a way that enforces immutability, versioning and retains a complete version history.
- **Pulled Automatically:** Software agents automatically pull the desired state declarations from the source.
- **Continuously Reconciled:** Software agents continuously observe actual system state and attempt to apply the desired state.

GitOps is a modern philosophy that builds upon other well established DevOps practices, such as:

- [Git](https://git-scm.com/)
- [Continous Integration](https://glossary.cncf.io/continuous_integration/)
- [Continuos Delivery](https://glossary.cncf.io/continuous_delivery/)
- [Configuration management](https://www.redhat.com/en/topics/automation/what-is-configuration-management)
- [Infrastructure as Code](https://glossary.cncf.io/infrastructure_as_code/)
- [Secret mangement](https://www.cyberark.com/what-is/secrets-management/)
- [Security](https://www.techtarget.com/searchsecurity/definition/security)
- [Observability](https://glossary.cncf.io/observability/)

The X or Anything as Code principle should be used wherever possible to make things manageable by GitOps principles. For Kubernetes this could be achieved the native way with CustomResources and CustomResourceDefinitions. Or more traditionally by using ConfigMaps, either with environment variables or configuration file.

GitOps is most often applied to Kubernetes environments, but is also utilized for IaC (Infrastructure as Code or on other platforms (Richardson, 2018). Weaveworks provides a [maturity model](https://www.weave.works/blog/the-gitops-maturity-model) for evaluating your organization&#39;s use of GitOps (Fremantle &amp; Buehrle, n.d.).

The next steps for Continous Delivery after implementing GitOps could be:

Progressive delivery by ataching release strategies to the GitOps workflow, such as [Blue/Green](https://glossary.cncf.io/blue_green_deployment/), [Canary](https://glossary.cncf.io/canary_deployment/) and [A/B Testing](https://www.optimizely.com/optimization-glossary/ab-testing/). Applying deployment validation by using [quality gates](https://blog.mirabeau.nl/en/articles/Automated_Quality_Gateways/5Taon4oabKUsmMuqmq66eS) that a deployment must adhere to before it's finalized. These quality gates can be implemented using SRE (Site Reliability Engineering) principles, such as [SLO's (service level objectives)](https://sre.google/sre-book/service-level-objectives/) and [SLI's (service level indicators)](https://sre.google/sre-book/service-level-objectives/). 

## 1.4. Security

**Research question:** What are the latest developments in the security world and how does it fit into a DevOps workflow?

The foundation of Cyber security begins with the CIA (Confidentiality, Integrity & Availability) triad. Confidentiality serves to keep out unauthorized individuals. Integrity is to prevent tampering and verify the origin of the source. Availability ensures data, systems and applications are accessible
ble when required.

### 1.4.1. Strategies

- **Defense in-depth** - A layered approach to security with fault tolerance in mind.
- **Security by design** - Security is not an afterthought and should be taken in to account within the desing proces.
- **privacy by design** The same about security could be said for privacy. In that minimal sensitive or personal identifiable data should be required.
- **Zero-trust architecture** - Never trust, always verify. Authenticate everything and everyone. 

### 1.4.2. Fameworks & compliance

Cyber security frameworks help organizations make sense of what areas of their IT landscape needs proper security measures for defending business assets. There are many cyber security frameworks, three categories are defined and would normally be achieved in the same order; control, program and risk frameworks source: [How to Make Sense of Cybersecurity Frameworks](https://www.youtube.com/watch?v=dt2IqidgpS4).

**Control framework** use cases:

- Identify set of baseline controls.
- Asses the state of of technical capabilities.
- Prioritize implementation of controls.
- Develop an initial roadmap for the security team.

**Program framework** use cases:

- Asses the state of the overall security program.
- Build a comprehensive security program.
- Measure maturity and conduct industry comparisons.
- Simplify communications with bushiness leaders.

Control and program frameworks can be used together and support each other. Mappings can be made between them.

**Risk framework** use cases:

- Definine key process steps for assesing and managing risk.
- Structure the risk management program.
- Identify, measure and quantify risk
- Prioritize security activities

Organizations that host frameworks for various of security domains:
- [CIS (Center for Internet Security)](https://www.cisecurity.org/)
- [NIST (National Institute of Standards and Technology)](https://www.nist.gov/)
- [ISO (International Organization for Standardization)](https://www.iso.org/)

The [Cloud Security Alliance (CSA) Cloud Controls Matrix (CCM)](https://cloudsecurityalliance.org/research/cloud-controls-matrix/) maps various security frameworks such as CIS, NIST, ISO etc. The CSA consilidates these various frameworks and offers a way to show the compliance level of your organization publicly.

Miscellaneous:
- [GDPR (General Data Protection Regulation)](https://gdpr.eu/), required by EU law.
- [NSA & CISA Kubernetes hardening guide](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/2716980/nsa-cisa-release-kubernetes-hardening-guidance/)

### 1.4.3. Domains

Security domains clarify which areas of your organization's security posture is a point of interest to defend. The most relevant security domains for the Joint Cyber Range are:

- Cloud security
- Kubernetes security
- Container security
- Application security
- CI/CD security
- Supply chain security

The CSA defined the following security domains.

![CSA (Cloud Security Alliance Secuirty Domains 1)](https://cloudsecurityalliance.org/packs/media/images/research/ccm/domains-left-48d9d3d3ca18958fbbcd743466b7f6c4.png)
![CSA (Cloud Security Alliance Secuirty Domains 2)](https://cloudsecurityalliance.org/packs/media/images/research/ccm/domains-right-b1dd50c5211416d8313f58ca4c2564a0.png)

## 1.5. Threat libraries

Compliance framework mostly try to improve security by checking of a list. In contrast to compliance frameworks, threat libraries can be used to identify real-world threats and mapping attack patterns to your IT infrastructure.

- [MITRE ATT&CK](https://attack.mitre.org/) matrix for Linux, Windows, Network, Containers, Kubernetes etc.

- [CWE (Common Weakness Enumeration)](https://cwe.mitre.org/)

- [CAPEC (Common Attack Pattern Enumerations and Classifications)](https://capec.mitre.org/)

### 1.5.1. Kubernetes

Kubernetes security is a subject on its own, by default Kubernetes is not secure.
Just like regular cyber security, various kinds of frameworks and guidance exist. Armosec compares the most prevalent ones in its [blog](https://www.armosec.io/blog/kubernetes-security-frameworks-and-guidance/). You should not use one or the other, but combine various ones to have complete coverage.

![Kubernetes Secuirty frameworks & guidance](https://uploads-ssl.webflow.com/5e0082cbe65e067bdadadff9/615da15210c2e8569ea665f7_guidenace%20post%20img.png)

### 1.5.2. DevSecOps

[DevSecOps](https://glossary.cncf.io/devsecops/) extends the [DevOps](https://glossary.cncf.io/devops/) culture with security practices. CI/CD, automated tests and Anything as Code are well-established DevOps practices. DevSecOps adds security gates through automated security testing and security as code principles. Because security is needed from beginning to the end, it becomes part of every step in the [DevOps cycle](https://www.tricentis.com/wp-content/uploads/2019/01/DevOps-cycle-Extended.png).

![DevSecOps Cycle](https://erepublic.brightspotcdn.com/dims4/default/9b8b3da/2147483647/strip/true/crop/1426x808+0+0/resize/1680x952!/format/webp/quality/90/?url=http%3A%2F%2Ferepublic-brightspot.s3.amazonaws.com%2F9a%2F22%2Fd17c7caf647337b95378752e0d0a%2Fgartner-graphic.png)

It is often described as shifting security left. Insights in all forms of code (app, config, infra, docs etc.) are provided early on in the development process to fix issues early and shorten the developer feedback-loop. 

Linting has an essential role in this and is the automated checking of your source code for programmatic and stylistic errors. This is done by using a lint tool, otherwise known as linter. A lint tool is a basic static code analyzer, mostly specialized in one language. A common occurrence is to use various tools bundled in one.

[MegaLinter](https://github.com/megalinter/megalinter) bundles linters from various languages. While the following project focusses on [Kubernetes validation tools](https://github.com/HighwayofLife/kubernetes-validation-tools).

Static analysis should be performed on code, scanning and validating it against:

- Best-practices
- Styling
- Bad code
- (Security) Bugs
- Insecure code
- Dependencies
- Vulnerabilities
- Leaking of secrets
- Compliance / hardening rules
- Set policies by the organization

As developer you want to know these things as soon as possible (with opt-out/in), and thus shortening the feedback loop and shifting security practices left. These static analysis scans would preferably be performed realtime within the IDE or as pre-commit hooks. Either way, before any bad code is send over the internet to centralized systems.

![securityscanning.png](Assets/securityscanning.png)

Static analysis require source code access and therefore is referred to as a whitebox, testing from the inside out. Static Analysis tools for Security fall in the category of [SAST (Static application security testing)](https://www.enso.security/appsec-glossary/sast) and is one of the first defense mechanisms against insecure coding practices. Note that this doesn't only apply to application security tests, but can also be applied to for example Kubernetes or Terraform code.

When deploying applications in an environment it can be exposed to new types of attacks that may not be visible from the source code. Those have to be tested using a blackbox method, from the outside in. [DAST (Dynamic application security testing)](https://www.enso.security/appsec-glossary/dast) scans the build or runtime environment of the application by probing it from the outside. IAST (Interactive application security testing) runs interactively and requires both source code access and outside access. Although, no open source IAST tools are available yet.

Note that security tests should be performed on various stages in the development lifecycle, not only in the IDE, pre-commit hooks or CI/CD. Preferably tests are run at every stage, also in production to evaluate active workloads.

OWASP launched a [DevSecOps maturity model](https://dsomm.timo-pagel.de/) for assesing the current DevSecOps practices of your organization against industry best-practices. Also, SANS recently released a [poster for Cloud Security and DevSecOps Best Practices](https://sansorg.egnyte.com/dl/OTrn2Tyq3n) and paints a complete picture to which you can check against your own organizations practices.

### 1.5.3. Supply chain security

Supply chain security aims to ensure no malicious code or workloads are run. This entails knowing what is in your code (dependencies & vulnerabilities) with SCA. Also to make sure code, artifacts, systems etc can be trusted and are not tempered with along the way to and even during production. This is mostly done by singing and verivying artifacts. Everything should be authenticated, even workloads. And thus, this fits into the "zero-trust" architecture.

The word `chain` refers to the whole Software Development Lifecycle, from development and the IDE, to deployment and operation in production. 

Since the SolarWinds and Kaseya supply chain attacks, the IT sector has been pushing developers and organizations on implementing supply chain security. Open source has a big role in this and affects large and important organizations. The U.S. government recently expressed its displeasure with open source security and the impact it has on organizations. 

Just like other subjects, a maturity model exists, this is called SLSA (Supply chain Levels for Software Artifacts) for assesing your current practices against industry best-practices.

CNCF projects to help with the software supply chain are:

- [Sigstore signing and verifying](https://www.sigstore.dev/)
- [SPIFFE/SPIRE workload identities](https://spiffe.io/)
- [TUF (The Update Framework)](https://theupdateframework.io/) / [Notary](https://github.com/theupdateframework/notary)

### 1.5.4. Tooling

**Consolidated Security dashboards:**

- [DefectDojo](https://www.defectdojo.org/) Vulnerability assessment and management tool
- [Faraday](https://faradaysec.com/) Collaborative pentesting
- [ArcherySec](https://www.archerysec.com/) vulnerability assessment and management tool 

**Kubernetes runtime security**

- [SecureCodeBox](https://docs.securecodebox.io/) Kubernetes based, modularized toolchain for continuous security scans.
- [Falco](https://falco.org/) Cloud-Native runtime security and de-facto Kubernetes threat detection engine
- [ThreatMapper](https://github.com/deepfence/ThreatMapper) Cloud-native runtime vulnerability Management and Attack Path Enumeration
- [Kubescape](https://github.com/armosec/) Kubernetes security assesment
- [Dev-Sec hardening framework](https://dev-sec.io/) Hardening automation
- [Starboard](https://github.com/aquasecurity/starboard) Kubernetes security assesment

**Security scanning tools:**

- [Trivy](https://github.com/aquasecurity/trivy) Container vulnerability scanner
- [OWASP Dependency track](https://dependencytrack.org/) Component Analysis platform
- [HuskyCI](https://huskyci.opensource.globo.com/) SAST for various programming languages
- [Chef Inspec](https://github.com/inspec/inspec) Compliance testing
- [OpenVas](https://www.openvas.org/) Vulnerability scanner
- [Gauntlt](http://gauntlt.org/) Integrates various security tools

# 2. Bibliography

RedHat. (n.d.). What is DevSecOps? Retrieved September 17, 2021, from [https://www.redhat.com/en/topics/devops/what-is-devsecops](https://www.redhat.com/en/topics/devops/what-is-devsecops)

Gienow, M. (2021). What Developers Need to Know About Kubernetes. [https://www.cockroachlabs.com/blog/what-developers-need-to-know-about-kubernetes/](https://www.cockroachlabs.com/blog/what-developers-need-to-know-about-kubernetes/)

Richardson, A. (2018). What Is GitOps. [https://www.weave.works/blog/what-is-gitops-really](https://www.weave.works/blog/what-is-gitops-really)

Johnson, P. (2021). Shift Left Testing: What Is It and What Are The Benefits. [https://www.whitesourcesoftware.com/resources/blog/shift-left-testing/](https://www.whitesourcesoftware.com/resources/blog/shift-left-testing/)

Joslyn, H. (2021). Stateful Workloads on Kubernetes with Container Attached Storage – The New Stack. [https://thenewstack.io/stateful-workloads-on-kubernetes-with-container-attached-storage/](https://thenewstack.io/stateful-workloads-on-kubernetes-with-container-attached-storage/)

Conway, S. (2018). Kubernetes is first CNCF project to graduate | Cloud Native Computing Foundation. [https://www.cncf.io/blog/2018/03/06/kubernetes-first-cncf-project-graduate/](https://www.cncf.io/blog/2018/03/06/kubernetes-first-cncf-project-graduate/)

Khayretdinov, A. (2018). The beginner&#39;s guide to the CNCF landscape | Cloud Native Computing Foundation. [https://www.cncf.io/blog/2018/11/05/beginners-guide-cncf-landscape/](https://www.cncf.io/blog/2018/11/05/beginners-guide-cncf-landscape/)

Cloud Native Computing Foundation. (n.d.). CNCF Cloud Native Interactive Landscape. Retrieved September 7, 2021, from [https://landscape.cncf.io/](https://landscape.cncf.io/)

Acedo, R., Tragler, A., &amp; Baudin, F. (2021). Run Your Kubernetes Cluster on OpenStack in Production - Superuser. [https://superuser.openstack.org/articles/run-your-kubernetes-cluster-on-openstack-in-production/](https://superuser.openstack.org/articles/run-your-kubernetes-cluster-on-openstack-in-production/)

Fremantle, P., &amp; Buehrle, A. (n.d.). The GitOps maturity model.
