---
title: README
description: 
published: true
date: 2022-01-31T19:39:57.857Z
tags: 
editor: markdown
dateCreated: 2022-01-31T19:31:32.183Z
---

# Deployment and Security project introduction

This project aims to provide research about Cloud-Native trends and state of the art technologies, mainly related to Kubernetes. With the goal of implementing reproducible, reliable and secure processes, supporting the web application and underlying infrastructure of the JCR (Joint Cyber Range).

Various documents for this innovation project can be found here:

[knowledge bits](knowledge_bits.md)
[product selection](product_selection.md)
[theory](theory.md)

See the following DevOps and Cloud trend report. The arrows show it's relevant for the JCR. Green arrows mean it's currently in effect, yellow means that it's partially in effect, and red means it has not yet been initiated yet.

![Assets/DevOpsCloudTrendsReport.png](Assets/DevOpsCloudTrendsReport.png)

If you read on you will find some of these trends reflected in the text, some of which are covered with more detail.

## Maturity models:

Maturity models are products of the IT industry and its community. They help to know where you stand as an organization in a certain area of IT and help to follow best practices. They help to mature your use of various technologies and concepts within your IT landscape by setting goals to improve upon.

Some stick to a simple poster, others offer something similar to how compliance frameworks apply to organizations. The more simpler ones are filled in to represent the JCR's maturity.

Note that anyone who thinks they understand a particular topic can create a maturity model or similar concept. Use the material wisely and don't blindly follow someone else's practices.

### [Kubernetes maturity model](https://www.fairwinds.com/kubernetes-maturity-model)

![Kubernetes maturity model](Assets/KubernetesMaturityModel.png)

### [Cloud Native Maturity Matrix](https://blog.container-solutions.com/cloud-native-maturity-matrix)

![CloudNativeMaturityMatrix.png](Assets/CloudNativeMaturityMatrix.png)

### [CD3M (Continuous Delivery 3.0 Maturity Model)](https://nisi.nl/continuousdelivery/articles/maturity-model)

![ContinuousDelivery3MaturityModel.png)](Assets/ContinuousDelivery3MaturityModel.png)

### [OWASP DSOM (DevSecOps Maturity Model)](https://dsomm.timo-pagel.de/spiderweb.php)

![OWASPDevSecOpsMaturityModel.png](Assets/OWASPDevSecOpsMaturityModel.png)

### [OWASP SAMM (Software Assurance Maturity Model)](https://owaspsamm.org/model/)

![OWASPSoftwareAssuranceMaturityModel.png](Assets/OWASPSoftwareAssuranceMaturityModel.png)

### [BSIMM (Building Security In Maturity Model)](https://www.bsimm.com/framework.html)

![BuildingSecurityInMaturityModel.jpg](Assets/BuildingSecurityInMaturityModel.jpg)

### [CII Best Practices](https://github.com/coreinfrastructure/best-practices-badge)

![CIIBadgeProgram.png](Assets/CIIBadgeProgram.png)
![opensffBestPracticesBadge.png](Assets/opensffBestPracticesBadge.png)

### [SLSA (Supply chain Levels for Software Artifacts)](https://slsa.dev/spec/v0.1/levels)

![https://lh5.googleusercontent.com/LCcLLTQ_obo0rNMsXTA4WVsmLparOGHfCUWgJDSkfDpGRIxo63jes0cywMw5w0qq3mQUIztCpRBdajOS_nLKy-JmU3KuoonZpsuVB-TEH6cQLzGVo-55TywF4J2eU_9q25Oeh5gL-zHC9Y4s5NcXX7msepZbAP8IFktVzdZoVejAnpYP=w692-h334](https://lh5.googleusercontent.com/LCcLLTQ_obo0rNMsXTA4WVsmLparOGHfCUWgJDSkfDpGRIxo63jes0cywMw5w0qq3mQUIztCpRBdajOS_nLKy-JmU3KuoonZpsuVB-TEH6cQLzGVo-55TywF4J2eU_9q25Oeh5gL-zHC9Y4s5NcXX7msepZbAP8IFktVzdZoVejAnpYP=w692-h334)

![SLSA Levels](https://lh5.googleusercontent.com/xImMHmA4tgKvLidz2YQEglPLz_Oz1ynorznP3mcRpGZ7zqBv4KeV4SDFXf4fROCIlm8qTBDhKemZHSJlXnZC_fck598S6lImbBpowD_PgP_PROV4ObqnjieqT3LP7b8kf97r_089c9LOp1FqRFPBvKKKMyr8J6EPHXLrsm5ZCruzqIJ8)

### [OpenSSF Security Scorecards](https://github.com/ossf/scorecard)

![test](https://www.zdnet.com/a/img/resize/1e416409b1e25ba4d793049a1b6b0afb77dd1470/2021/07/01/51943e71-d7c1-43b6-ad0b-102af9e2b9d2/image-8.jpg?width=1200&height=1200&fit=crop&auto=webp)
The Open Source Security Foundation's Security Scorecards currently only supports public GitHub repos. That said, you could still manually use the [checklist](https://github.com/ossf/scorecard#scorecard-checks). Note that the previous CII Best Practices badge is a check for OpenSSF Security Scorecards.

### [Best-practice-for-network-segmentation](https://github.com/sergiomarotco/Network-segmentation-cheat-sheet)

This project was created to publish the best practices for segmentation of the corporate network of any company. In general, the schemes in this project are suitable for any company.

- [Level 1 of network segmentation: basic segmentation](https://github.com/sergiomarotco/Network-segmentation-cheat-sheet#level-1-of-network-segmentation-basic-segmentation)
- [Level 2 of network segmentation: adoption of basic security practices](https://github.com/sergiomarotco/Network-segmentation-cheat-sheet#level-2-of-network-segmentation-adoption-of-basic-security-practices)
  - Note the relevance of previously mentioned maturity models DSOMM & SLSA.
- [Level 3 of network segmentation: high adoption of security practices](https://github.com/sergiomarotco/Network-segmentation-cheat-sheet#level-3-of-network-segmentation-high-adoption-of-security-practices)
- [Level 4 of network segmentation: advanced deployment of security practices at scale](https://github.com/sergiomarotco/Network-segmentation-cheat-sheet#level-4-of-network-segmentation-advanced-deployment-of-security-practices-at-scale)

### [SANS SWAT checklist](https://www.sans.org/cloud-security/securing-web-application-technologies/?msc=cloud-security-lp)

![SecuringWebApplicationsChecklist.jpg](Assets/SecuringWebApplicationsChecklist.jpg)

### [SANS Cloud Security and DevSecOps Best Practices](https://www.sans.org/posters/cloud-security-devsecops-best-practices/)

![CloudSecurityDevSecOpsBestPractices.jpg](Assets/CloudSecurityDevSecOpsBestPractices.jpg)