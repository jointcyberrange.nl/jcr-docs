---
title: CTFd plugins schrijven
description: 
published: true
date: 2022-01-14T08:38:21.888Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:01:01.861Z
---

# CTFd plugins schrijven 

CTFd biedt de mogelijkheid om plugins te schrijven. Deze kunnen makkelijk geïmplementeerd worden in het systeem zonder al teveel moeite.

# Opzet van plugin architectuur

Om de plugin architectuur te behouden van de standaarden van CTFd is het belangrijk om de volgende structuur te behouden:

```plaintext
CTFd
└── plugins
   └── CTFd-plugin
       ├── README.md          # README bestand
       ├── __init__.py        # Ingangspunt van CTFd die geladen wordt
       ├── requirements.txt   # Met requirements die gedownload moeten worden
       └── config.json        # Plugin configuratie bestand
```

Dit is standaard voor een simpele plugin om te hebben. Er is maar één bestand hier verplicht en dat is de *\_\_init\_\_.py* file. Deze heb je nodig om je plugin werkend te krijgen.

Laten we even een snelle voorbeeld plugin schrijven die iets print naar de console. Maak een folder aan in je CTFd plugins folder met de naam van de nieuwe plugin. In dit voorbeeld noem ik deze “testplugin”. Maak hier in deze nieuwe folder die je gecreëerd heb, de *\_\_init\_\_.py* file aan. De architectuur ziet er als volgt uit.

```plaintext
CTFd
└── plugins
   └── testplugin
       └── __init__.py 
```

Om een simpele print te krijgen bij het opstarten van de plugin schrijf het volgende in de *\_\_init\_\_.py* file:

```plaintext
def load(app):
    print("De test plugin werkt")
```

Je hebt nu een simpele plugin die bij het opstarten van de applicatie (of tijdens auto-refresh als je in een development environment draait) in de terminal het volgende staan:

```plaintext
dev-env | De test plugin werkt!
dev-env |  * Loaded module, <module 'CTFd.plugins.testplugin' from '/workspaces/CTFd-master/CTFd/plugins/testplugin/__init__.py'>
```

Zoals je ziet leest het CTFd platform de plugin en voegt deze meteen toe. In de opstart zie je alle plugins toegevoegd worden waaronder de test plugin hierboven aangemaakt.

# Configuratie pagina

Plugins wil je graag kunnen aanpassen via een user interface waar nodig, je wilt dit niet in de code hardcoded moeten aanpassen. Om deze via het admin panel te kunnen bereiken moet je een *config.json* maken waarin je de naam aangeeft en de route voor de pagina. In de test plugin voeg in de volgende *config.json* toe aan mijn plugin folder:

```plaintext
{
    "name": "Test plugin",
    "route": "/admin/plugins/testplugin"
}
```

Nu is het probleem dat als we in het admin panel op plugins > testplugin klikken, dat we een 404 pagina error krijgen. Dit willen we voorkomen en hiervoor moeten wij onze eigen pagina maken. Standaard door CTFd wordt het pad *{URL}*/admin/plugins/*{PluginFolderNaam}* gevuld als je een bestand maakt genaamd *config.html*.

Als je nu een *config.html* file maakt in je plugin folder dan wordt dat je standaard configuratie pagina. Laten we nu proberen om een simpele pagina weer te geven. Dit schrijven we in de *config.html*:

```plaintext
<h1>Voorbeeld</h1>
<p>Dit is een voorbeeld pagina</p>
```

Als je dit opent in je browser zie je dat je een pagina krijgt zonder enige stijl van CTFd. Daarom moet je gebruik maken van een van de templates van CTFd. Hier gebruiken wij de standaard CTFd admin panel template. Deze is te vinden in de folder CTFd > themes > admin > templates. Hier vind je de *base.html*. Dit is de template die wij gaan gebruiken en die gebruik je als volgt:

```plaintext
{% extends "admin/base.html" %}

{% block content %}
<div class="jumbotron">
	<div class="container">
		<h1>Test Plugin</h1>
	</div>
</div>
<div class="container">
    <h1>Voorbeeld</h1>
    <p>Dit is een voorbeeld pagina</p>
</div>
{% endblock %}
```

Nu heb je een simpele configuratie website in het thema van CTFd. Als je alles goed heb gedaan zou een standaard pagina zien met de admin panel navigatie balk en een header 1 met Voorbeeld en een paragraaf met “Dit is een voorbeeld pagina”.

# Een nieuwe route toevoegen

Een nieuwe pagina toevoegen aan het CTFd platform is praktisch gezien gewoon een nieuwe flask route schrijven. De plugin kan worden gebruikt in de hele applicatie.

In onze load functie die we hadden in de *\_\_init\_\_.py* file gaan we nu een app route toevoegen. Laten wij in dit geval een /faq pagina vragen (dit zijn *frequently asked questions*).

```plaintext
from flask import render_template

def load(app):
    print('De test plugin werkt!') 
    @app.route('/faq', methods=['GET'])
    def view_faq():
        return render_template('page.html', content="<h1>FAQ Page</h1>")
```

Zoals je hier ziet importeer je de render\_template functie van flask, aangezien CTFd bijna alle pagina’s regelt via templates. De oude load functie met de print hadden we nog staan, deze hebben we alleen maar uitgebreid met de view\_faq functie met natuurlijk de @app.route decorator zodat flask afhandelt dat het ook daadwerkelijk een pagina word. Door deze decorator moet de functie waar die boven staat wel een pagina returnen. Nou doen we dat via de render\_template functie die ons een pagina terug geeft. Hier hebben we gekozen voor de *pagina.html* van het core theme. Dit is standaard en geeft een mooie schone (niet admin) pagina.

# Een bestaande route modificeren

Dit is iets complexer dan het vorige kopje en dat komt doordat flask het eigenlijk niet support om een bestaande route de overschrijven met je eigen route.

Voor dit voorbeeld ga ik de oude code uit onze *\_\_init\_\_.py* weg doen om weer met een schoon fragment te kunnen programmeren.

Wij gaan maken dat als mensen naar de challenges pagina gaan dat er komt te staan dat hier momenteel aan gewerkt wordt en er nog geen pagina’s beschikbaar zijn.

```plaintext
from flask import render_template

def load(app):
    def view_challenges():
        return render_template('page.html', content="""
        <h1>Challenges zijn momenteel niet beschikbaar</h1>
        <p>Wij zijn momenteel bezig met nieuwe challenges maken</p>
        """)

    app.view_functions['challenges.listing'] = view_challenges
```

Wat we hier doen is net zoals hiervoor maken we gebruik van de return\_template functie om ervoor te zorgen dat we een functie hebben die onze pagina terug geeft. Wat we vervolgens doen is we passen de functie die nu eigenlijk de /challenges pagina weergeeft, passen wij aan zodat die onze functie gaat gebruiken en passen zo verder niks aan aan de source code van CTFd. Omdat we in de load de app meegeven kunnen we deze hier gebruiken. De app kent alle functies en waar die aan toegekent zijn.

Hoe we bijvoorbeeld erachter zijn gekomen dat we hier de string ‘challenge.listing’ moesten gebruiken doe je als volgt. We gaan in de volgende file: /CTFd/challenges.py. Hier kun je zien dat er een listing() functie is die ervoor zorgt dat er een template wordt gereturned. Hierdoor weten wij dus nu dat wij deze functie willen overschrijven en dat geven wij mee aan de app.

# Registreren van assets

Vaak voor het maken van een iets grotere pagina in CTFd heb je misschien extra CSS of JS nodig. Deze kan je niet zomaar meegeven in de render\_template functie. Dus wat je kan doen is in je plugin folder een nieuwe folder aanmaken genaamd assets (of een naam naar keuze).

Hier is een voorbeeld van hoe je zo’n folder toekent als assets folder:

```plaintext
from CTFd.plugins import register_plugin_assets_directory

def load(app):
    # Available at http://{CTFd-URL}/plugins/testplugin/assets/
    register_plugin_assets_directory(app, base_path='/plugins/testplugin/assets/')
```

Hier is een voorbeeld van hoe je één file als asset toekent

```plaintext
from CTFd.plugins import register_plugin_asset

def load(app):
    # Available at http://{CTFd-URL}/plugins/testplugin/assets/file.js
    register_plugin_asset(app, asset_path='/plugins/testplugin/assets/file.js')
```

Dit is handig om te gebruiken en is eigenlijk standaard bij elke plugin gemaakt voor CTFd.