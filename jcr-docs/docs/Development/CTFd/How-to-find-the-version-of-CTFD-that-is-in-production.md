---
title: How-to-find-the-version-of-CTFD-that-is-in-production
description: 
published: true
date: 2022-12-05T16:54:17.472Z
tags: 
editor: markdown
dateCreated: 2022-12-05T16:22:16.396Z
---

# How to find the version of CTFD that is in production

## **CTFD finding the container version**

1. go to the CTFD website of your deployed tenant

2. log in as an administrator
![login-page.png](Assets/login-page.jpg)

3. navigate to the admin panel
![admin-panel.png](Assets/admin-panel.jpg)

4. scroll to the bottom of any page in the admin panel and look at the container id
![containerID.png](Assets/containerID.jpg)

5. you probably want to match this to the version in your container registry. In the gitlab registry this is called the "Manifest digest".

## **Where can I find this in the code**

### path: ../ctfd-jcr/CTFd/\_\_init__.py

![containerID-code.png](Assets/containerID-code.jpg)

This happens in the **create_app** function that initializes the application. On the first three lines the namespace of the deployed container is retrieved.

The lines after this retrieve a token from the kubernetes secrets. The namespace and pod name retrieved from the environment are formatted into an url. The url gets combined with the token into an api call to retrieve the container data. In the container data the imageID is found and split to only use the manifest code. Finally, the manifest code is stored in the config where it can be called from the utils.

## **Where can I find this in the html**

### path: ../ctfd-jcr/CTFd/themes/admin/templates/base.html

![containerID-html.png](Assets/containerID-html.jpg)

This is the footer of all the admin pages. On line **138** you see the that the container version is retrieved by the utils function **get_config** followed by the key **ctf_container_id**.
