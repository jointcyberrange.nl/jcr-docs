---
title: CTFd tenant aanvraag docenten
description: 
published: true
date: 2022-02-01T08:26:28.169Z
tags: 
editor: markdown
dateCreated: 2022-02-01T08:22:03.020Z
---

# CTFd tenant aanvraag voor docenten
Zie onderstaande link om een tenant aanvraag in te dienen als docent bij de JCR

https://hogeschoolutrecht.sharepoint.com/:w:/r/sites/Eerstemeeting-Project180-HU-InstituteforICT/Gedeelde%20documenten/Project%20INNO%20258/Stefan/Issue%20295%20Werkinstructie%20URL%20en%20CTF%20tenant.docx?d=w05104c9501034b49a071e705e53f8c20&csf=1&web=1&e=WJjnYg