---
title: Needed storage for tenants
description: 
published: true
date: 2022-01-14T08:38:24.066Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:01:06.192Z
---

# Needed storage for tenants

Tenants need storage for their challenges. The amount needed has been measured with the method described below.

## Instructions for measuring the disk usage

MariaDB

```plaintext
kubectl exec  -it -n ctf-platform2 mariadb-5dc65d95cb-t2wmd -- du -h /var/lib/mysql
```

CTFd logs

```plaintext
kubectl exec  -it -n ctf-platform2 ctfd-78d9b87ccc-fb9vb -- du -h /var/uploads
```

CTFd Uploads

```plaintext
kubectl exec  -it -n ctf-platform2 ctfd-78d9b87ccc-fb9vb -- du -h /var/uploads
```

## Storage used by freshly installed

- MariaDB-database: 131MB

- CTFd uploads folder: 0B

- CTFd logs folder: 0B

## Storage use with 60 Users, 105 Challenges and 0 uploads

- MariaDB-database: 131M

- CTFd logs: 8KB

- CTFd uploads: 0B

## Storage use with 60 Users, 106 Challenges and 12 uploads of various sizes and file extensions

- MariaDB-database: 134MB

- CTFd uploads folder: 0B (probably faulty test)

- CTFd-uploads: 4.7GB

## Advice amount of storage per tenant

For the database, I would recommend around 2GB of diskspace, which isn’t a lot in storage terms, but it IS a lot for the CTFd-database. This accounts for possible grow of disk space.

For the logging: the system wasn’t used a lot, and therefore the logs didn’t grow that much. This should be tested automatically on a live instance.

For the uploads: for this test, a realistic situation was simulated with files of different sizes and file Extensions. The extensions were plausible files for use in a CTF-challenge. I would recommend:

- Challenge catagories like forensics and reverse engineering would likely have higher storage capacity requirements since they use big files in challenges. For these categories of challenges, I would recommend at least 20 GB.

- For other challenges, I would recommend 10-15 GB.

### Upload limit NGINX for bigger files

When uploading big files (like those for forensics challenges) you might run into the file size limit for uploads. Currently, the default limit is set in the following Kubernetes ingress-NGINX annotations for every ingress-resource deployed.

```plaintext
nginx.ingress.kubernetes.io/proxy-body-size: "1024m" nginx.org/client-max-body-size: "1024m
```

## Future improvements

- More realistic measurement of the log-data. This should be monitored on a live CTFd-instance. This can be done with: [https://github.com/yashbhutwala/kubectl-df-pv](https://github.com/yashbhutwala/kubectl-df-pv) or with the Longhorn dashboard.

- For scalability of file uploads, you could use the native S3-compatible API integration within CTFd. Each tenant could have its own bucket or bring their own Bucket. Maybe External S3-compatible providers like CloudFerro’s object storage or CloudFlare R2 could be a cheaper option?
  
according to [https://blog.ankursundara.com/uiuctf21/amp/](https://blog.ankursundara.com/uiuctf21/amp/) this makes a lot of difference in load on CTFd, quote:

> Don't use CTFd's default static file serving - CTFd supports S3 for hosting file uploads/assets. Use this! It is very beneficial to keep load off of CTFd for serving file downloads. We thought we would have to make an entirely separate plugin for file assets, but luckily Google cloud platform has an S3 compatability mode for its Cloud Storage. We set that up, and only had to make a 1-line change to CTFd to get it to work.
