<!--
title: Logging with Loki
description: 
published: true
date: 2022-01-19T15:12:58.329Z
tags: 
editor: ckeditor
dateCreated: 2021-12-21T14:01:41.103Z
-->

# Logging with Loki&nbsp;
For logging in our Kubernetes we use the Grafana Loki stack, this includes Loki as logging backend, promtail for the logging agent/collector and Grafana as frontend.

Loki is described as “Like Prometheus, but for logs“, in the sense that it uses Kubernetes labels in the same way as Prometheus does. Loki aims for cost effectiveness and easy operation.

## Installation
We use the following Helm installation, but is managed by ArgoCD:

helm upgrade --install loki grafana/loki-stack --namespace=logging --set promtail.enabled=true,loki.persistence.enabled=true,loki.per
sistence.storageClassName=longhorn,loki.persistence.size=5Gi

```bash
helm upgrade --install loki grafana/loki-stack --namespace=logging --set promtail.enabled=true,loki.persistence.enabled=true,loki.persistence.storageClassName=longhorn,loki.persistence.size=5Gi
```

The Loki data source can now be added in Grafana with the following URL or can be done automatically with a ConfigMap for Grafana: [http://loki.logging.svc.cluster.local:3100](http://loki.logging.svc.cluster.local:3100)

See it live at: [https://grafana.dev-jointcyberrange.nl/](https://grafana.dev-jointcyberrange.nl/)

## Usage

### Grafana

Click on `Explore` in the menu on the left:

![explore](Assets/explore.png)

Select `Loki` as the data source to explore in the top left corner:

![select-loki](Assets//select-loki.png)

Click on the `Log browser` button:

![log-browser](Assets/log-browser.png)

Select the desired Kubernetes labels to extract logs from:

![kubernetes-label](Assets/kubernetes-label.png)

Click on the `Show logs` button:

![show-logs](Assets/show-logs.png)

Your logs will now appear:

![logs](Assets/logs.png)

### LogCLI

The LogCLI is an alternative interface that can interact with the Loki API. To access Loki with the LogCLI, Loki will have to be exposed. Loki doesn’t support a native authentication method and has to be implemented in combination with a reverse proxy or other technique. For this, we use the NGINX Ingress Controller and enabled HTTP Basic Authentication.

Install the LogCLI by following the documentation at: [https://grafana.com/docs/loki/latest/getting-started/logcli/](https://grafana.com/docs/loki/latest/getting-started/logcli/)

The LogCLI can be used as follows:

```bash
export LOKI_ADDR=https://loki.dev-jointcyberrange.nl
export LOKI_USERNAME=loki-username
export LOKI_PASSWORD=loki-password
logcli --tls-skip-verify query '{app_kubernetes_io_name="ingress-nginx"}'
```