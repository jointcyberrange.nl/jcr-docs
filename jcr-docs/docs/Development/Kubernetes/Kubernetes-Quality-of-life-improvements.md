---
title: Kubernetes Quality of life improvements
description: 
published: true
date: 2022-01-14T08:38:36.118Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:01:29.438Z
---

# Kubernetes Quality of Life improvements 

Kubernetes is often described as a platform to build platforms on top of. This entails that Kubernetes by itself is pretty bare-bone and empty, you have to make it your own. When it comes to development and operating a cluster, tooling can help abstracting the complicated layers and assist in the tedious tasks.

## Helm

Helm is often described as the package manager for Kubernetes, like apt is for Debian/Ubuntu. It packages or bundles various Kubernetes manifests to deploy multiple application components in Kubernetes. Popular applications are availible as Helm charts in public repositories, like container images on Docker Hub. [Artifacthub.io](http://Artifacthub.io) hosts various Cloud-Native artifacts availible to the community, including Helm charts. Install it for your system with the following [link](https://helm.sh/docs/intro/install/)

## K9s

When deploying to Kubernetes, `kubectl get pods` will be run often. K9s replaces this command with a nice dashboard in your terminal. It also provides a log viewer and quick port forward shortcut. Install it for your system with the following [link](https://github.com/derailed/k9s). The command `k9s` will spawn the terminal dashboard. Availible shortcuts are shown at the top. Cluster resources can be navigated with the `arrow keys` and selected with `Enter`, press `esc` to exit a selected resource and `Ctrl+C` for exiting the application. Press `0` to show all namespaces. Your're also able to quickly port-forward a container and edit and delete resources,.

## Lens

When in need of something more feature-rich and complete, Lens the Kubernetes IDE can be used. This looks more like the Rancher dashboard used by the JCR in production. With the big difference being that Lens runs on your workstation. It utilizes your Kubeconfig for finding and authenticating to your cluster and only provides access to resources provided in your RBAC (Roll Based Access Control) configuration. Lens makes it more easy to mange multiple clusters and provides a general overview of resources. Lens automatically captures basic Prometheus metrics and also provides Helm integration and mangement.

My use cases for Lens are; more quickly identify issues with its logging capabilities and manually editing/deleting resources, for when i'm tinkering. Because of all its capabilities, Lens is described as the Kubernetes IDE. Install it for your system with the following [link](https://k8slens.dev/)

Your Kubeconfig should be used acording to the documentation, but hasn't in my case. Add a cluster manually by clicking `File`, `Add cluster` and providing a kube-config file. This is located in your user's profile directory, in the `.kube` folder and `config` file. Note that all Kubernetes clusters in your Kube-config file will be added to lens. Click on your local cluster and connect, when your're connected you can pin it to the Hotbar. Explore your cluster its components and resources. Lens also provides extensions, relevant for the JCR are:

- [Resource map visualized](https://github.com/nevalla/lens-resource-map-extension)

- [Debugging pods](https://github.com/pashevskii/debug-pods-lens-extension)

- [Chaos Engineering gamified](https://github.com/chenhunghan/lens-ext-invaders)

Press `Ctrl+Shift+E` as shortcut to add an extension. Copy/paste the extension URL e.g. `@nevalla/kube-resource-map` and install the extension.

## Aliases

For common commands u can use aliases to shorten them. WSL2 simulates Linux on Windows, I use an Ubuntu distribution as my main terminal interface. You can add aliases to `~/.bashrc` with your favorite text editor and activate them with the following command `. ~/.bashrc`. I've added the following to the end of my `.bashrc` file:

```plaintext
alias k='kubectl'
alias kgn='kubectl get nodes'
alias kgp='kubectl get pods'
alias kgpa='kubectl get pods -A'
alias kgs='kubectl get service'
alias kdes='kubectl describe'
alias kl='kubectl logs'
alias ka='kubectl apply -f'
alias kdel='kubectl delete'
alias kdf='kubectl delete -f'
```

The following command is an one liner to add an entry to the end of your `~/.bashrc` file.

`echo "YourAlias test='YourCommand'" >> /home/user/.bashrc && . ~/.bashrc`

## Tips and miscellaneous

### Kubectl autocompletion

Enable kubectl autocompletion in your Bash terminal with:

source <(kubectl completion bash) echo "source <(kubectl completion bash)" >> ~/.bashrc

### Namespace separation

Namespaces are a simple Kubernetes concept for separation. They're the most basic implementation to separate users, although not necessarily recommended. Namespace separation is mostly applied on an application level.

### Working with multiple Kubernetes configs & cluster contexts

See current cluster contexts:

```plaintext
kubectl config get-contexts
```

Create cluster contexts for the development & production cluster with:

```plaintext
export KUBECONFIG=$KUBECONFIG:kube_config_dev.yaml:kube_config_prod.yaml
```

Check if the cluster contexts are updated:

```plaintext
kubectl config get-contexts

CURRENT   NAME           CLUSTER        AUTHINFO                  NAMESPACE
*         demo-cluster   demo-cluster   kube-admin-demo-cluster
          dev-cluster    dev-cluster    kube-admin-dev-cluster
```

Use a cluster context:

```plaintext
kubectl config use-context dev-cluster

CURRENT   NAME           CLUSTER        AUTHINFO                  NAMESPACE
          demo-cluster   demo-cluster   kube-admin-demo-cluster
*         dev-cluster    dev-cluster    kube-admin-dev-cluster
```