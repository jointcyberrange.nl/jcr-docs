---
title: Kubernetes Troubleshooting Tips
description: 
published: true
date: 2022-01-19T15:12:56.395Z
tags: kubernetes
editor: markdown
dateCreated: 2021-12-21T14:01:37.185Z
---

# Kubernetes troubleshooting tips 

### Lens

Lens is often described as the IDE for Kubernetes. It provides a great overview of cluster resources and minimal monitoring (if turned on). Accessing logs of pods is easy and warnings/errors are shown at the cluster overview tab.

### Namespace stuck in termination

When a namespace is stuck in termination you can resolve it with the following oneliner. It will ask you to enter the name of the corresponding namespace you want to delete.

```Bash
echo 'namespace to delete: ' && read && kubectl get namespace ${REPLY} -o json | jq '.spec = {"finalizers":[]}' | kubectl replace --raw "/api/v1/namespaces/${REPLY}/finalize" -f -
```

Finalizers are a protection for related resources within a namepace. The oneliner will empty the `finalizers` array. This oneliner however requires `jq` to be installed.

You can install jq with:

```Bash
sudo apt-get install jq
```

### Spawn a DNS troubleshooting pod

The following command can be used to spawn a pod with DNS utilities for troubleshooting purposes. When you exit the pod will be deleted.

```Bash
kubectl run -it --rm --image anubhavmishra/tiny-tools dnstools`
```

### Delete Evicted Kubernetes pods

Evicted pods can relate to various porblems. A common one is resource limitations and various services competing for consumption of those resources. Resource constraints should be set in place for Kubernetes pods. [https://support.huaweicloud.com/intl/en-us/cce\_faq/cce\_faq\_00209.html](https://support.huaweicloud.com/intl/en-us/cce_faq/cce_faq_00209.html)

![](Assets/pod-eviction.png)

You can delete evicted pods as follows, you have to specify the namespace twice.  
This command can be more effiecient, e.g. iterating over namespaces or setting values. Do you know how? Feel free to publish an improvement!

```Bash
kubectl get pods -n {namespace} | grep Evicted | awk '{print $1}' | xargs kubectl delete pod -n {namespace}
```

### Ingress-nginx deployment error

Redeployment of the ingress-nginx controller might fail on a cluster to which it has been deployed before, with a error like this:

```Bash
helm upgrade --install ingress-nginx --create-namespace --namespace=ingress-nginx -f ./dont/ingress-nginx-helm-values.yaml ingress-nginx/ingress-nginx
Release "ingress-nginx" does not exist. Installing it now.
Error: rendered manifests contain a resource that already exists. Unable to continue with install: ClusterRole "ingress-nginx" in namespace "" exists and cannot be imported into the current release: invalid ownership metadata; annot
ation validation error: missing key "meta.helm.sh/release-name": must be set to "ingress-nginx"; annotation validation error: missing key "meta.helm.sh/release-namespace": must be set to "ingress-nginx"
```

If this happens, you can create a YAML-file containing the deployment as it would be ran by Helm and then delete it’s contents with `kubectl` (and remove the temporary YAML-file afterwards):

```Bash
helm template ingress-nginx --create-namespace --namespace=ingress-nginx -f ./dont/ingress-nginx-helm-values.yaml ingress-nginx/ingress-nginx > nginx.yaml
kubectl delete -f nginx.yaml
rm nginx.yaml
 
```