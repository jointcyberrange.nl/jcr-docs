---
title: Overdrachtsdocument
description: 
published: true
date: 2022-02-08T07:13:43.499Z
tags: 
editor: markdown
dateCreated: 2022-02-08T07:13:43.499Z
---

## Inleiding

Dit document is geschreven voor project 258: Joint Cyber Range. Binnen dit document leggen wij alle informatie vast dat nodig is om het project over te kunnen dragen naar een ander projectteam. Zo hebben zij alle informatie direct beschikbaar zonder hier op verschillende platformen naar hoeven te zoeken. Ook staan hier bekende bugs en fouten in waarvan het handig is om deze te weten voordat er gewerkt gaat worden aan het project. Ook staan de huidige product backlog in dit document zodat er een idee opgedaan kan worden naar activiteiten die nog moeten gebeuren.

## Context

Joint Cyber Range wil een leerplek creëren voor studenten van alle niveaus om informatie op te doen over hacking. Dit gebeurt door challenges te voltooien die je op kan lossen doormiddel van verschillende hacking methodes toe te passen op de challenges.
Een set van challenges zal worden geleverd door JCR zelf. Ook kunnen docenten zelf challenges aanmaken die beter passen bij hun eigen omgeving en niveau. Hiermee kunnen docenten originele uitdagingen creëren voor studenten wat een verfrissend uitzicht geeft op de leermethodes dat studenten beschikbaar zullen hebben.
Waar begin ik?

- Iedereen: bekijk het platform, lees de WikiJS
- SD’er: doe de getting-started, lees de developer FAQ
- BIM’er: gebruik de roadmap om inspiratie op te doen voor mogelijke User Stories
- CSC’er: bekijk de Admin FAQ en voor inspiratie van User Stories de roadmap.


## Artefacten

### CTFd

CTFd is het hoofdproduct van dit project. Dit wordt gebruikt als CTF platform van de JCR. Momenteel draait er CTFd versie 3.4.0. Meer uitleg hierover is te vinden in de documentatie.

### Plugins

Er bestaan momenteel 3 custom CTFd plugins die zijn ontwikkeld door de JCR:

- custom_auth
- initial_setup
- container_challenges
Ook zijn er 2 aangepaste CTFd plugins die zijn ontwikkeld door CTFd:
- dynamic_challenges
- challenges

### OWASP Juice Shop

De OWASP Juice Shop is een website die bedoeld is om te hacken, voor het verbeteren van je Cyber Security kennis met als thema web exploitation in verschillende categorieën. Het is gemaakt door OWASP (Open Web Application Security Project) wat zich hard maakt voor het voorlichten omtrent web kwetsbaarheden en bewustwording aanbrengt bij bedrijven en ontwikkelaars. De Juice Shop komt standaard met een aantal challenges, deze zijn uit te rollen doormiddel van de CTFd backup importfunctie. Dit gaat door middel van JSON-bestanden, waardoor het ook mogelijk is om eigen gemaakte challenges toe te voegen.

### Docker base image

Voor base image hebben we voor nu gekozen voor het besturingssysteem Debian 10 Buster. Debian is een Linux distributie zonder veel onnodige software of bloatware, is stabiel en wordt lang ondersteund (King, 2020). Alpine is een besturingssysteem wat veel gebruikt wordt om de image grootte te minimaliseren, echter kan dit tot het tegenovergestelde effect leiden of bugs veroorzaken (Turner-Trauring, 2020).
Momenteel gebruikt de CTFd container als base image python:3.7-slim. Dit is een minimale variant van de officiële Python Debian 10 Buster image op Docker Hub <https://hub.docker.com/_/python>.
De officiële Python image gebruikt een aantal packages die niet vereist zijn voor het draaien van de CTFd applicatie. Het is echter momenteel te veel overhead om eigen images te bouwen en wordt deze gebruikt als base image.
Voor MariaDB is een image op basis van de officiële image gebouwd die gebruik maakt van het secrets-management platform Doppler. Voor Redis wordt gebruik gemaakt van de officiële image.

### Ansible

Configuratie van een nieuwe server kan automatisch met het Ansible Playbook. Dit maakt de server gereed voor gebruik, zodat RKE up uitgevoerd en de Kubernetes cluster gebruikt kunnen worden. Daarnaast wordt Ansible ook ingezet om de VM's up-to-date te houden. De playbooks zijn terug te vinden in deze repository.

### Kubernetes

Kubernetes wordt gebruikt voor container orkestratie en het automatisch uitrollen van microservices in de vorm van containers. Het is deels platform onafhankelijk en kan daardoor op verschillende soorten infrastructuur worden uitgerold bij verschillende cloud aanbieders. De Kubernetes distributie kan hier verschillen en resulteren op aanpassingen.
Momenteel wordt in het platform gebruik gemaakt van de Kubernetes distributie RKE (Rancher Kubernetes Egine). De deployment van RKE op de VM's van Surf is vastgelegd in deze repository.

### NGINX Ingress

De ingress binnen Kubernetes is het ingangspunt voor alle applicaties die op de cluster draaien. Het is mogelijk om iets anders te gebruiken dan NGINX zoals HAproxy. Over de NGINX ingress is het meeste te vinden, doordat deze het meeste wordt gebruikt in combinatie met Kubernetes. De NGINX ingress wordt geïnstalleerd middels de Kubernetes package manager Helm. De applicatie is gedeployed via ArgoCD via deze Gitlab repo.

### Cert-manager (Let’s Encrypt TLS certificaten)

Cert-manager is de aanbevolen certificate issuer voor Kubernetes en wordt gebruikt voor het aanvragen van gratis Let’s Encrypt TLS certificaten. Cert-manager vereist zo goed als geen configuratie, het email adres wat wordt gebruikt voor de aanvraag van certificaten is te wijzigen op <https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize/-/blob/main/apps/cert-manager/base/clusterissuer.yaml>. De applicatie is gedeployed via ArgoCD via deze Gitlab repo.

### Calico CNI

Calico wordt gebruikt gebruikt als CNI (Container Network Interface) provider en wordt geïnstalleerd middels de RKE-configuratie.
SURFconext
In het Service Provider Dashboard is een applicatie te vinden die gebruik maakt van de test omgeving van SURFconext <https://sp.surfconext.nl/>. Dit is geïmplementeerd in het CTFd platform doormiddel van het OpenID connect protocol. Het is noodzakelijk toegevoegd te worden aan het team om toegang te krijgen tot dit dashboard <https://teams.surfconext.nl/teams/40356/members>.

### OpenStack

OpenStack zou worden gebruikt voor het testen van Kypo, hier is het team niet meer aan toe gekomen. Doormiddel van de onderstaande repository maakt het mogelijk OpenStack te installeren doormiddel van Ansible <https://gitlab.com/hu-hc/jcr/openstack/kolla-ansible>.

### Wiki.js

Wiki.js wordt gebruikt om documentatie op te kunnen slaan. Hiervoor werd hier Confluence voor gebruikt maar dat is huidig aangepast naar Wiki.js. Dit is een open-source programma om documentatie vast te kunnen leggen. Wiki.js wordt ook huidig in productie bij ons gebruikt en is bereikbaar op: <https://docs.jointcyberrange.nl/>
De opdrachtgever zal nieuwe leden handmatig toe moeten voegen doordat hier geen email-server aan gekoppeld is.

### Hubspot

Hubspot is een ticketing systeem dat gebruikt wordt door JCR. Hiervoor kreeg de opdrachtgever vaak mailtjes binnen met diverse soorten verzoeken. Met de nieuwe Hubspot omgeving kunnen mensen makkelijk via een formulier hun verzoek inschieten, en kan de opdrachtgever deze toewijzen aan verschillende mensen. Ook hier kan nog een e-mailserver toegevoegd worden zodat mensen gecontacteerd kunnen worden vanuit Hubspot. Ook hier moet de opdrachtgever u handmatig aan toevoegen.

### ArgoCD

ArgoCD is gebruikt voor de deployment van alle applicaties in zowel de staging- als productieomgeving van JCR. De werking van ArgoCD is vastgelegd in Wiki.js. Verdere ontwikkeling vindt plaats in de Gitlab repo.

### Doppler-operator

De Doppler-operator wordt gebruikt om secrets uit Doppler beschikbaar te maken voor de applicaties die in de Kubernetes clusters draaien. De applicatie is gedeployed via ArgoCD via deze Gitlab repo.

### Gitlab pipeline

Voor de CTFd repo is er een Continuous Integration (CI) pipeline voor het bouwen en testen van de CTFd image. De pipeline staat gedefinieerd in .gitlab-cml.yml. Bij elke push op de master branch wordt een trivy dependency uitgevoerd. Je kan er ook nog een handmatige linter test uitvoeren, maar die duurt een half uur. Het bouwen van de image is momenteel ook nog handmatig omdat je op die manier images kan scheiden in de Container Registry.

### Gitlab Runners

Via ArgoCD worden er Gitlab Runners geregistreerd die draaien in de kubernetes omgeving van JCR. Deze runners worden gebruikt om images te bouwen voor CTFd en de Challenges, maar ook om tests mee uit te voeren van de CI-pipeline.

### Renovate

Renovate wordt gebruikt om dependencies van een aantal van JCR's repo's bij te werken. De applicatie is gedeployed via ArgoCD via deze Gitlab repo. De applicaties die worden bijgewerkt via Renovate zijn vastgelegd in Wiki.js. Een beschrijving van Renovate is ook vastgelegd in Wiki.js.

### Monitoring

Voor JCR is een monitoring stack ingericht op basis van Prometheus en Grafana. De applicaties zijn gedeployed via ArgoCD via deze Gitlab repo.

## Documentatie

Er is documentatie geschreven op meerdere plekken. Voor algemene documentatie wordt Wiki.js gebruikt. Om te starten met het CTF platform, lees de getting-started. Er is ook specifieke documentatie over het CTF platform. Om een overzicht te hebben van alle Gitlab repositories, ga naar de Gitlab Group. Alle overige documentatie staat in de Microsoft Sharepoint.

## Inloggegevens van DB en Servers

De inloggegevens zijn vertrouwelijk en kunnen opgevraagd worden bij de opdrachtgever. Deze staan opgeslagen in Doppler waar de opdrachtgever teamleden aan kan toevoegen.

## Deployment

Er is nu ene mogelijkheid om omgevingen per tenant uit te rollen (hier aangegven met meerdere exemplaren van de deployment).
![deployment.png](assets/deployment.png)
Om beter te kunnen bekijken:
<https://hogeschoolutrecht.sharepoint.com/sites/Eerstemeeting-Project180-HU-InstituteforICT/Gedeelde%20documenten/Project%20INNO%20258/Oplevering/deployment.png>

## Known bugs en beperkingen

**Challenge import – backup functie**

- Bij het importeren van challenges via de backup functie wordt de bestaande data verwijderd.  

**Getting started lokaal niet bereikbaar via browser icm. VMware**

- De lokale omgeving draait op port 443, vmware heeft een deprecated functie die ook port 443 gebruikt. Er is documentatie geschreven hoe je dit probleem kan oplossen.

**Container challenge stoppen**

- Als je een container challenge stopt en de challenge wegklikt, kun je bij het opnieuw openen van de challenge nog steeds de deployment informatie zien. Dit komt omdat het verwijderen van een k8s namespace erg lang kan duren.

**Asset folders kunnen niet met hoofdletter aangemaakt worden** 

- Als je een asset folder binnen wiki.js aanmaakt wordt dit automatisch zonder hoofdletter gemaakt, maar door een asset folder via Visual Studio Code aan te maken, kan je er een aanmaken met hoofdletter. Hierdoor zijn er soms dubbele asset folders die alleen verwijderd kunnen worden via VSC.

## Product backlog

- Als Cloud Engineer wil ik alternatieve containerruntimes onderzoeken, zodat het containerplatform toekomst vast is
- Als systeembeheerder wil ik een back-up kunnen maken van het JCR-platform, zodat ik deze backup terug kan zetten
- Als cloud-engineer wil ik dat de bestaande YAML-configuraties worden ontdaan van onnodige configuratie zodat deze overzichtelijker worden.
- Als Docent wil ik challenges van de TS101 podcast kunnen selecteren, zodat de student interessante challenges heeft.
- Als beheerder wil ik logging hebben zodat ik kan nagaan waarom het deployen van docker images niet werkt in de getting started omgeving.
- Als student wil ik een challenge hebben waarbij ik gebruik kan maken van een OWASP top 10 vulnerability, zodat ik daarmee kan oefenen.
- Als systeemeigenaar wil ik dat de functionaliteit om submissions naar CSV te kunnen exporteren wordt ondergebracht in een CTFd plugin, zodat de functionaliteit op een logischere plek staat.
- Als opdrachtgever wil ik nieuwe CTFd pagina's kunnen uitrollen door middel van de initial_setup_plugin, zodat ik geen aanpassingen in de CTFd GUI hoef te doen.
- Als beheerder wil ik dat er automatische test worden uitgevoerd na het bouwen van de base image zodat problemen vroeg worden gerapporteerd.
- Als beheerder wil ik dat de CI pipeline verschillende images maakt zodat de dev en productie omgeving gescheiden kunnen worden.
- Als beheerder wil ik een advies over het gebruik van helm voor de gitlabrunners zodat er geen docker meer wordt gebruikt bij het JCR-platform.
- Als opdrachtgever wil ik iedere maand een volledige redeploy van de devcluster kunnen uitvoeren, zodat in het geval van een disaster de volledige omgeving weer opgebouwd kan worden
- Als opdrachtgever wil ik dat HTTP logging gerelateerd wordt aan users, zodat ik kan zien wat users allemaal doen.
- Als opdrachtgever wil ik weten welke overdrachtsdocumentatie belangrijk is, zodat ik zelf en de volgende groep gemakkelijk het werk kan voortzetten
- Als opdrachtgever wil ik dat het innovatieplan wordt bijgewerkt zodat er een recent overzicht is van het innovatieproject
- Als opdrachtgever wil ik een overzichtelijke mapstructuur hebben voor de overdrachtdocumenten, zodat hier gemakkelijk informatie uitgehaald kan worden
- Als operator wil ik ArgoCD Notifications toepassen in de staging-cluster, zodat ik een waarschuwing krijg als er iets misgaat in de omgeving.
- Als beheerder wil ik een browsertest toevoegen in de CI pipeline zodat de functionaliteiten van de JCR website getest kunnen worden.
- Als beheerder wil ik dat de challenge repo's gebruik gaan maken van kaniko voor het bouwen van images zodat er maar 1 techniek voor het bouwen van images wordt gebruikt.

## Bronnen

CTFd. (sd). Deployment. Opgehaald van CTFd Documentation: <https://docs.ctfd.io/docs/deployment/>
Jankov, T. (2020, september 23). MariaDB vs. MySQL, twee verschillende database-technologieën. Opgehaald van Kinsta: <https://kinsta.com/nl/blog/mariadb-vs-mysql/>
King, B. (2020, 01 31). 12 Reasons Why You Should Choose Debian Linux. Opgehaald van makeuseof: <https://www.makeuseof.com/tag/reasons-choose-debian-linux/>
surfnet. (sd). Schema en uitleg - SURFconext - Get Conexted - SURF Wiki. Opgehaald van surfnet: <https://wiki.surfnet.nl/display/surfconextdev/Schema+en+uitleg>
Turner-Trauring, I. (2020, 08 19). Using Alpine can make Python Docker builds 50× slower. Opgehaald van pythonspeed: <https://pythonspeed.com/articles/alpine-docker-python/>
