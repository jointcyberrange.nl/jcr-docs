---
title: CI/CD pipeline overview
description: 
published: true
date: 2022-02-01T08:45:28.689Z
tags: ci/cd, gitlab ci, argo cd, artifacts
editor: markdown
dateCreated: 2022-01-31T21:16:22.997Z
---

# CI/CD pipeline overview

## Continuous Integration

The Jointcyberrange(JCR) has a CI pipeline on [Gitlab](https://www.gitlab.com) for the [CTFd](https://gitlab.com/hu-hc/jcr/platform/ctf-platform) repository.

### Gitlab Pipeline

#### **Overview**

![ci-pipeline.png](Assets/ci-pipeline.png)

The Gitlab pipeline consists of 2 stages, the test stage and build stage. The test stage is partly automated. The `trivy_dependency` job executes everytime a change is pushed to the repo, and makes an artifact to download. This artifact expires in 30 days. The `mega_linter` job is set on manual execution, for it takes around 30 min to compleet. It also make an artifact to download, but the artifact expires in a week. The build stage is also manual, this is so you can manualy assign it a version number or place it in a specific folder in the [Container Registry](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/container_registry). For a guide check [here](how-to-build-a-container-image.md).

The pipeline is defined in the `.gitlab-cml.yml` or `.gitlab-ci.yml` file. For the basics and how to add a job click [here](how-to-add-a-job.md).

#### **Download an artifact**

1. Go to the [pipeline](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/pipelines) overview on gitlab.
![pipeline-artifacts1.png](Assets/pipeline-artifacts1.png)
2. Select the 3 small dots.
![pipeline-artifacts2.png](Assets/pipeline-artifacts2.png)
3. Select the artifact to download.

### Gitlab Runners

If the `.gitlab-cml.yml` or `.gitlab-ci.yml` file is the brain of the CI pipeline than the Runners are the muscles. They spring into action when a CI pipeline starts. The Gitlab Runner can take up to 10 jobs at once. If you need more than that you can change the [configuration](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize/-/blob/main/apps/gitlab-runner/base/runner.yaml) or register more runners.
The active Gitlab Runners can be found [here](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/settings/ci_cd?expand_runners=true).
> You must have the maintainer or higher rol for the repository to see the Runner page above.
> There is an active [issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27367) with the runners, it may take a while sometimes to pick up a job. Please be patient.

> DO NOT reset the registration token! Otherwise ArgoCD can not register a new runner!

## Continuous Delivery

### ArgoCD

![cd_pipeline.png](Assets/cd_pipeline.png)

ArgoCD is the CD solution for JCR. The documentation can be found [here](Argo_cd.md).
