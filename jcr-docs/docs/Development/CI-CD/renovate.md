---
title: Renovate
description: renovate is an automated dependency updater used in multiple JCR-repositories.
published: true
date: 2022-10-07T11:47:11.932Z
tags: 
editor: markdown
dateCreated: 2022-01-11T10:43:34.825Z
---

# Renovate
How it works:
- renovate runs every day in the staging cluster (cron)
- it currently scans the following repositories:
	- '31796437' # HU-HC / Joint Cyber Range / Platform / argocd-kustomize
  - '21146237' # HU-HC / Joint Cyber Range / Platform / CTFd
  - '23998417' # HU-HC / Joint Cyber Range / Platform / Kubernetes Cluster Deployment
- If it finds an out-of-date dependency, renovate creates a pull request that needs to be either merged or deleted by a developer or an operator.

The deployment of renovate is defined in **HU-HC / Joint Cyber Range / Platform / argocd-kustomize / apps / renovate**.