---
title: Install gitlab runner on kubernetes
description: 
published: true
date: 2022-01-14T16:54:21.402Z
tags: 
editor: markdown
dateCreated: 2021-12-22T13:21:47.455Z
---

# **Introductie**

Dit is een guide voor het installeren en registreren van een GitLab runner op Kubernetes. In productie wordt dit d.m.v. ArgoCD gedaan, deze guide kan gebruikt worden voor een lokale omgeving.

## **Prerequisites**

- Een Kubernetes cluster.
- [Helm](https://helm.sh/) op het systeem(Host) dat Kubernetes draait.
- SSH verbinding naar de Host.
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) die geconfigureerd is voor de Kubernetes cluster.
- Een Gitlab repo waar je de Runner aan wilt toevoegen. De token is te vinden onder **Settings > CI/CD > Runners**

## **Installing the GitLab Runner**

1. SSH naar de Host waar het k8 cluster draait.
2. kopieer de `values.yaml` naar de host.
    Verplicht zijn de opties:
    - `gitlabUrl` - the GitLab server full URL (e.g., [gitlab.example.com](https://gitlab.example.com)) to register the runner against.
    - `runnerRegistrationToken` - The registration token for adding new runners to GitLab.
    - `tags` - de tags die je aan de gitlab runner wilt meegeven om op te pakken
    - `name` - de naam van de gitlab runner

    Hieronder een voorbeeld van `values.yaml`:

    ```yaml
    gitlabUrl: https://gitlab.com/
    runnerRegistrationToken: [TOKEN]
    rbac:
      create: true
    runners:
      tags: ["TAG1, TAG2"]
      name: ["NAME"]
      locked: false
    ```

3. Voeg de GitLab Helm repo toe:

    ```sh
    helm repo add gitlab https://charts.gitlab.io 
    ```

4. En installeer de runner:

    ```txt
    helm install --namespace <NAMESPACE> --create-namespace gitlab-runner -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner 
    ```

    als voorbeeld, in de gitlabci namespace en met de `values.yaml`:

    ```txt
    helm install --namespace gitlabci --create-namespace gitlab-runner -f values.yaml gitlab/gitlab-runner 
    ```
