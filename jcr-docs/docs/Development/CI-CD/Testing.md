---
title: Testing
description: 
published: true
date: 2022-01-19T15:12:48.390Z
tags: 
editor: markdown
dateCreated: 2021-12-30T17:21:52.856Z
---

# Testing

Testing is an importent part within the DevOps workflow. We want to test the code we develop to make sure it functions properly.
Preferably, these tests are automated and run continuously on every code commit. Quality gates on these tests indicate whether they passed, failed and if the pipeline succeded in the end.

Note that tests are not exclusive to applications only. Infrastructure, services or configuration can also be tested. Even documentation can be tested for if it's following styling guides or best practices.

Testing can be done on various levels of the application and is often visualized using a test pyramid.

![Test automation pyramid](https://i1.wp.com/lakedatainsights.com/wp-content/uploads/2018/12/TestAutomationPyramid.jpg?ssl=1)

Various kinds of tests have different charesteristics associated with them. Agile testing quadrants diagrams try to visualize these charesteristics.

![Agile testing quadrant](https://lisacrispin.com/wp-content/uploads/2011/11/Agile-Testing-Quadrants.png)

Testing is done by using a test framework. These can fall in one of several categories. [Read more about that here](https://www.softwaretestinghelp.com/test-automation-frameworks-selenium-tutorial-20/)

## Static analysis & Code quality

Before any tests are ran, static analysis must be performed on the code. This ensures that the quality of the code is up to standards, mitigating:

- Bad code
- Styling errors
- General bugs
- Security bugs
- Misconfiguration

## Unit tests

Unit testing for Python application is often done with one of two libraries Unittest or Pytest. See a comparison [here](https://www.pythonpool.com/python-unittest-vs-pytest/) of the two. 

## Component tests


## Integration tests


## E2E tests

E2E (End-2-end) tests with proper metrics done continuously in production is the same as synthetic monitoring. This way you're assured of user-level functionallity working properly.

[Selenium](https://www.selenium.dev/) is a popular testing framework that automates browsers. Besides coding the tests manually, Selenium also provides the Selenium IDE for a GUI click and test experience. Tests can be integrated to run within the CI/CD pipeline.

## SRE (Site Reliabillity Engineering)

SRE is a concept coined by Google and focusses on engineering systems to be more reliable and predictable. Monitoring, logging, observabillity, alerting and incident response are important components of SRE. For more information read the [Google SRE Workbook](https://sre.google/workbook/table-of-contents/).

### Load & performance testing

Application should perform normally under load. When the application is deployed in the staging environment, a load test should be performed where performance is continuously measured. These results can be taken into account when performing [capacity planning](https://www.linkedin.com/pulse/sre-concepts-part-5-capacity-planning-availability-monitoring-koert/).

[K6](https://k6.io/) is an open souce load testing tool and cloud service. Just as Selenium , K6 offers GUI based click and test experience with the k6 browser recorder. Tests can be integrated to run within the CI/CD pipeline.

### Chaos engineering

Chaos Engineering is the discipline of experimenting on a system in order to build confidence in the system’s capability to withstand turbulent conditions in production. Chaos engineering tests the fault tolerance and ability of a system to self-heal and remediate issues.

## Security testing

### SAST (Static Application Security Testing)

SAST requires access to source code and uses static analysis to find various security issues. Note that it can also be applied to for example Kubernetes manifests.

### SCA (Software Composition Analysis)

SCA requires access to source code and scans it for dependencies, licenses, vulnerabilities and more. This information is used to create a SBOM (Single Bill of Material), which is a list of all dependencies and vulnerabilities in your source code and the ones they inherent from their dependencies.

Vulnerabilities from dependencies are carried over in the software supply chain as shown in the picture.

![supply_chain_vulnerabillity.png](Assets/supply_chain_vulnerabillity.png)

### DAST (Dynamic Application Security Testing)

DAST requires the application to be operational. Security test are ran with a black-box approach from the outside in.

### Penetration testing

With penetration testing runtime components are tested against known malicious attacks. This is done with a black-box approach from the outside in and performed either manually or automatic. 