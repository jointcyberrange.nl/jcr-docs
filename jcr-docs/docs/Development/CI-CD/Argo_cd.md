---
title: Argo CD
description: Argo CD is the continous delivery tool we use to deploy all of our applications to the Kubernetes clusters
published: true
date: 2022-01-31T11:11:28.873Z
tags: kubernetes, argo cd
editor: markdown
dateCreated: 2022-01-25T09:36:53.457Z
---

# Argo CD
This page contains relevant links to maintain and develop our Argo CD deployment. We use [Argo CD Autopilot](https://github.com/argoproj-labs/argocd-autopilot) to deploy our applications to the Kubernetes clusters running on the Surf-VM's. Furthermore, this page describes situations that occured during the project and how we dealt with them using ArgoCD. 

## 1. relevant links
[Argo CD repo](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize). This repository contains the documentation on how deploy Argo CD to our clusters and further develop the environment.

[Argo CD UI](https://argocd.dev-jointcyberrange.nl). Through the Argo CD UI, you can manage and maintain all of our applications running in the Kubernetes clusters. Obtaining the credentials for the UI are documented in [Doppler](../../Beheer/Doppler.md).

[Updating-applications](Updating-applications.html). All of our applications are deployed via Argo CD. This page contains documentation on how to update each one, leveraging Argo CD's GitOps capabilities.

## 2. Situations
### 2.1 Delete pod
This one occurs the most. For example, when you want to force a new version of CTFd in Kubernetes, you can delete the pod through Argo and the new one will be pulled from the repo. Here's how you can do it:
First, go to the [Argo CD UI](https://argocd.dev-jointcyberrange.nl). From there, go to projects en select the project where the application resides in:
![delete_pod_1.png](delete_pod_1.png)
> You can use the Firefox/Chromium extension "dark reader" to get dark mode in the UI


Next, select the project where you want to delete the pod, in my case it's staging-ctfd:
![delete_pod_2.png](delete_pod_2.png)

In this screen, you can see all the resources in the staging project that belong to ctfd. In the [Argo CD repo](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize), the path for this application would be apps/ctfd/overlays/staging, where ctfd is the application and staging the project. In this screen, we can choose to delete one of the resources. In this case, we're deleting the CTFd pod to force an update. Locate the resource you're trying to delete and press the three dots:
![delete_pod_3.png](delete_pod_3.png)

Now we can delete the pod. Because the deployment expects one pod, a new one is launched immediately.
![delete_pod_4.png](delete_pod_4.png)

Now the updated version of CTFd is running in the staging cluster.

### 2.2 delete application
Sometimes we want to start over with a fresh deployment. Throught Argo CD, we can delete an entire application. Thought self-healing, Argo CD will relaunch the application from scratch. This was used plenty of times through the project. Here's how to do it:
> This will delete all persistent data. Make sure to backup any relevant data first!
{.is-warning}

The first two steps are the same as the previous situation. Filter on the project where the application is running in and select the application. You should be on the screen where all the resources for the application are displayed. Next, we can delete the application by pressing "delete" (yep):
![delete_application.png](delete_application.png)
This opens the delete screen. Enter the name of the application, staging-ctfd in this case. Leave the propagation policy on foreground, that way it will delete all resources within the application. Argo CD will now delete all resources and the screen automatically closes once it is deleted. After a little while, the application will be rebuild. 

### 2.3 get application logs
Sometimes an application deploys with errors. Using Argo CD, we can figure out why be accessing application logs through the UI. Here's how to do it: 
The first two steps are the same as the previous situation. Filter on the project where the application is running in and select the application. You should be on the screen where all the resources for the application are displayed. Next, we go to the resource from which we want to retrieve logging. For this example, we will use staging-ctfd again. Click on the resources (not on the three dots):
> You can detect resources that are failing by their health status. Broken resources are usally in a Progressing or Degraded state.


![application_logs_1.png](application_logs_1.png)

Now we are on this screen:
![application_logs_2.png](application_logs_2.png)

Here we can see the manifest that was used to the deploy the resource, amongst other handy information. However, we are here to retrieve the application logs. Click LOGS and this screen opens:
![application_logs_3.png](application_logs_3.png)
Here we so a live stream of the logging of CTFd running in staging. Could be handy to troubleshoot any possible misconfiguration. Here's an example:
`[ERROR] [2022-01-31 08:53:52] value of ENV variable "SETUP_EVENT_MODE" cannot be invalid!`



