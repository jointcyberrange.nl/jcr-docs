---
title: Hoe moet je een job toevoegen aan de CI pipeline?
description: 
published: true
date: 2022-01-21T13:00:51.802Z
tags: 
editor: markdown
dateCreated: 2022-01-21T12:24:35.653Z
---

# Hoe moet je een job of test toevoegen aan een GitLab CI/CD pipeline?

## Yaml formatting

Het .gitlab-ci.yml of .gitlab-cml.yml is een yaml bestand. Let op de spaties in de syntax! Ansible heeft een mooie pagina gemaakt met alle basics er op. [YAML basics](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)

## Stages

Een Stage is een deel van de pipeline. Dit deel bestaat uit 1 of meerdere jobs. Hieronder is een voorbeeld van 2 stages in een .gitlab-ci.yml file.

```yaml
stages:
  - static_tests
  - build
```

## Jobs

Een [job](https://docs.gitlab.com/ee/ci/jobs/) is de basis van de [CI/CD pipeline](https://docs.gitlab.com/ee/ci/pipelines/). In een job moet er een **script** parameter zitten! Er kunnen meerdere jobs in een stage zitten, dit doe je door de stage parameter hetzelfde te houden.
Hieronder zie je 3 jobs die allemaal onder dezelfde stage vallen, door de nummering worden ze gegroepeerd in de pipeline overview.

```yaml
build ruby 1/3:
  stage: build
  script:
    - echo "ruby1"

build ruby 2/3:
  stage: build
  script:
    - echo "ruby2"

build ruby 3/3:
  stage: build
  script:
    - echo "ruby3"
```

## Script

De [script](https://docs.gitlab.com/ee/ci/yaml/#script) parameter is **verplicht** om te gebruiken in een job en kan bestaan uit een single line of multi line commando.

```yaml
job1:
  script: "bundle exec rspec"

job2:
  script:
    - uname -a
    - bundle exec rspec

job3:
  script:
    - |
      if ..
      else ..
      fi
```

## Tags

De [tags](https://docs.gitlab.com/ee/ci/yaml/#tags) parameter wordt gebruikt om aan te geven welke GitLab runners de job kunnen oppakken.
Hieronder een voorbeeld van een job die opgepakt kan worden door runners met de tags: ruby en postgres.

```yaml
job:
  tags:
    - ruby
    - postgres
```

## Image

De [image](https://docs.gitlab.com/ee/ci/yaml/#image) parameter wordt gebruikt om te specificeren welk docker image(of een ander image) gebruikt kan worden om de job uit te voeren. Hieronder gebruiken we het kaniko image om images te bouwen. Zoals je ziet kan je ook een entrypoint mee geven.

```yaml
image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]

```

## Variables

De [variables](https://docs.gitlab.com/ee/ci/yaml/#variables) parameter wordt gebuikt om variabelen mee te geven in een job. Dit kan zowel in de Global space als in de job space.

```yaml
# Global space
variables:
  DEPLOY_SITE: "https://example.com/"

deploy_job:
  stage: deploy
  script:
    - deploy-script --url $DEPLOY_SITE --path "/"

deploy_review_job:
  stage: deploy
  # job space
  variables:
    REVIEW_PATH: "/review"
  script:
    - deploy-review-script --url $DEPLOY_SITE --path $REVIEW_PATH
```

## When

De [when](https://docs.gitlab.com/ee/ci/yaml/#when) parameter kan je gebruiken om te bepalen wanneer je een job wilt laten uitvoeren. De meest gebruikte opties zijn:

- *manual*, start de job alleen na een handmatige trigger.
- *always*, start de job altijd ook als een vorige stage is gefaald.

```yaml
deploy_job:
  stage: deploy
  script:
    - make deploy
  when: manual
```

## Rules

De [rules](https://docs.gitlab.com/ee/ci/yaml/#rules) parameter kan worden gebruikt om te bepalen of een job moet worden meegenomen in de pipeline. Het voorbeeld hieronder zorgt ervoor dat de job alleen gedaan wordt als er een push is naar de default branch.

```yaml
rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: always
```

## Allow_failure

De [allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure) parameter kan aan een job worden toegevoegd om te zorgen dat als de job faalt dat de pipeline wel kan doorgaan met draaien. Er komt dan passed te staan met een uitroepteken, i.p.v. een vinkje.

```yaml
job2:
  stage: test
  script:
    - execute_script_2
  # is een boolean: true of false(default)
  allow_failure: true
```

## Include

De [include](https://docs.gitlab.com/ee/ci/yaml/#include) parameter wordt gebruikt om externe YAML files toe te voegen aan je pipeline. Deze file kunnen zowel local als remote zijn. Je kan door dezelfde job naam in je locale .gitlab-ci.yml te zetten de basis configuratie aanpassen.

```yaml
include:
  - remote: 'https://jobs.r2devops.io/trivy_dependency.yml'

trivy_dependency:
  # deze remote gaat nu dus met de tag master een runner opzoeken.
  tags:
    - master
  stage: static_tests
  # hiermee worden alleen de critical events gelogged.
  variables:
    TRIVY_SEVERITY: "CRITICAL"
  allow_failure:
    # dit is een manier om error codes af te vangen.
    exit_codes:
      - 1
```

## Artifacts

De [artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) parameter is om de resultaten van een test makkelijk te kunnen downloaden van github.com. Let hierbij op dat het path klopt dat je opgeeft want anders kan die de file niet uploaden!

```yaml
pdf:
  script: xelatex mycv.tex
  artifacts:
    # kijkt standaard in de working dir
    paths:
      - mycv.pdf
    # je kan opgeven hoelang de artifact geldig is.
    expire_in: 1 week
```

## Bronnen

Wil je meer te weten komen over jobs en tests? [Hier](https://docs.gitlab.com/ee/ci/) is de link naar de gitlab docs pagina.
