---
title: How to build an image
description: 
published: true
date: 2022-01-19T15:12:52.338Z
tags: 
editor: markdown
dateCreated: 2021-12-23T11:52:33.807Z
---

# How to build a container image

## **Running the pipeline**

1. Go to the repo on [gitlab](https://gitlab.com) where you want to build an image from.

2. On the sidebar press the **CI/CD** button.
	![firefox_2021-12-23_12-25-30.png](Assets/CICD-Pipeline.png)

3. In the top right corner press the **Run pipeline** button.
	![firefox_2021-12-23_12-25-46.png](Assets/Run-pipeline.png)

4. Select the branch to build an image from.

5. The default value for the `FODLER` variable is development. This entails every pipeline that runs will push to the development folder unless you override using:
		
    - Copy `FOLDER` into the `Input variable key` field.
    
    - Add the name of the folder (eg. master) for the container image as value in the `Input variable value` field.

6. The default container image tag used is `latest`. You can override the default behavior with:
    
    - Copy `CI_COMMIT_TAG` into the `Input variable key` field.

    - Add the name of your container image tag as value in the `Input variable value` field.
	![firefox_2021-12-23_12-26-35.png](Assets/Input-variable-value.png)

7. Press the **Run pipeline** button to start the pipeline.

8. Press the **play** button next to `kaniko-build` to start the build job.
	![firefox_2021-12-23_12-27-48.png](Assets/Update-kaniko-build.png)
