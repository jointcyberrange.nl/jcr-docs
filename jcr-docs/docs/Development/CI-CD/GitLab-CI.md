---
title: GitLab CI
description: DevOps pipeline using GitLab CI
published: true
date: 2022-01-14T21:55:51.076Z
tags: ci/cd, pipeline, devops, gitlab ci
editor: markdown
dateCreated: 2022-01-14T18:10:00.220Z
---

Get started with GitLab CI/CD, it can automatically build, test, and deploy your application.

The pipeline stages and jobs are defined in a ```.gitlab-ci.yml``` file. You can edit, visualize and validate the syntax in this file by using the Pipeline Editor.

The JCR does not use all of these features.

---

[How to build a container image](how-to-build-a-container-image.md)
[GitLab runner Kubernetes](gitlab-runner-kubernetes.md)
Theory for [testing](Testing.md)
[Updating applications](Updating-applications.html) using [Renovate](renovate.md)

---

💡 Tip: Visualize and validate your pipeline

Use the Visualize and Lint tabs in the Pipeline Editor to visualize your pipeline and check for any errors or warnings before committing your changes.

---

⚙️ Pipeline configuration reference

Resources to help with your CI/CD configuration:

* Browse [CI/CD examples and templates](https://gitlab.com/help/ci/examples/index)
* View [.gitlab-ci.yml syntax reference](https://gitlab.com/help/ci/yaml/index)
* Learn more about [GitLab CI/CD concepts](https://gitlab.com/help/ci/index)
* Make your pipeline more efficient with the [Needs keyword](https://gitlab.com/help/ci/yaml/index#needs)

---

:cloud: Use [R2DevOps hub](https://r2devops.io/jobs/) for pre-defined GitLab jobs in various categories, some even work right out-of-the-box.

The following jobs could be used within the technology stack of the JCR or try to use the [R2Devops hub generator](https://pipeline.r2devops.io/). This explains how to [get started](https://r2devops.io/use-the-hub/#quick-setup) manually.

## Static tests

* [gitleaks](https://r2devops.io/jobs/static_tests/gitleaks/ "https://r2devops.io/jobs/static_tests/gitleaks/") - A ready-to-use trivy job to scan the application dependencies of your code
* [trivy_dependency](https://r2devops.io/jobs/static_tests/trivy_dependency/ "https://r2devops.io/jobs/static_tests/trivy_dependency/") - A ready-to-use trivy job to scan the application dependencies of your code
* [sls_scan](https://r2devops.io/jobs/static_tests/sls_scan/ "https://r2devops.io/jobs/static_tests/sls_scan/") - Use a ShiftLeftSecurity sast scan to secure your application
* [links_checker](https://r2devops.io/jobs/static_tests/links_checker/ "https://r2devops.io/jobs/static_tests/links_checker/") - A job that helps you find broken links in your Markdown and HTML files
* [mega_linter](https://r2devops.io/jobs/static_tests/mega_linter/ "https://r2devops.io/jobs/static_tests/mega_linter/") - A job that helps you find broken links in your Markdown and HTML files
* [super_linter](https://r2devops.io/jobs/static_tests/super_linter/ "https://r2devops.io/jobs/static_tests/super_linter/") - A ready-to-use combination of various linters, to help validate the quality of your source code
* [spell_check](https://r2devops.io/jobs/static_tests/spell_check/ "https://r2devops.io/jobs/static_tests/spell_check/") - Analyze your .md files to find spelling mistakes
* [owasp_dependency_check](https://r2devops.io/jobs/static_tests/owasp_dependency_check/ "https://r2devops.io/jobs/static_tests/owasp_dependency_check/") CLI or owasp dependency track GUI - Analyze your dependencies for various packet managers using Dependency-Check
* [python_test](https://r2devops.io/jobs/static_tests/python_test/ "https://r2devops.io/jobs/static_tests/python_test/") - no unit tests yet - A ready-to-use pipenv and pytest environment to launch your unit tests

## Dynamic tests

* [zaproxy](https://r2devops.io/jobs/dynamic_tests/zaproxy/ "https://r2devops.io/jobs/dynamic_tests/zaproxy/") - A ready-to-use job to run a Dynamic Application Security Testing using Zaproxy
* [newman](https://r2devops.io/jobs/dynamic_tests/newman/ "https://r2devops.io/jobs/dynamic_tests/newman/") - Run a Postman collection to test your API with Newman
* [testssl](https://r2devops.io/jobs/dynamic_tests/testssl/ "https://r2devops.io/jobs/dynamic_tests/testssl/") - Tool to check SSL/TLS related vulnerabilities
* [lighthouse](https://r2devops.io/jobs/dynamic_tests/lighthouse/ "https://r2devops.io/jobs/dynamic_tests/lighthouse/") - A job using Google Lighthouse to do various tests on a website, and giving you a comprehensive report
* [trivy_image](https://r2devops.io/jobs/dynamic_tests/trivy_image/) -   A ready-to-use trivy job to scan your docker images and discover dependency vulnerabilities
* [nmap](https://r2devops.io/jobs/dynamic_tests/nmap/ "https://r2devops.io/jobs/dynamic_tests/nmap/") - A port scan job using nmap to alert on unwanted services and open ports on your server

## Provision

* [gitlab-terraform_plan](https://r2devops.io/jobs/provision/gitlab-terraform_plan/ "https://r2devops.io/jobs/provision/gitlab-terraform_plan/") - This job will initialize a working directory containing Terraform configuration files and plan the changes

## Release

* [semantic_release](https://r2devops.io/jobs/release/semantic_release/ "https://r2devops.io/jobs/release/semantic_release/") - Get your versioning & release deployments automated using semantic-release

## Deploy

* [aws_s3_sync](https://r2devops.io/jobs/deploy/aws_s3_sync/ "https://r2devops.io/jobs/deploy/aws_s3_sync/") - A ready-to-use AWS S3 job to sync a local directory into a bucket. This Job is compatible with all S3 Object storage.
* [ssh](https://r2devops.io/jobs/deploy/ssh/ "https://r2devops.io/jobs/deploy/ssh/") - A simple job to run a remote command through SSH
