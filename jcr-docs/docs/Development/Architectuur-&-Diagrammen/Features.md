---
title: Features
description: 
published: true
date: 2022-01-14T14:37:24.574Z
tags: 
editor: markdown
dateCreated: 2022-01-14T14:37:22.655Z
---

# Features

De Joint Cyber Range maakt gebruik van CTFd als score voor het platform en breidt hierop uit met door studenten ontwikkelde uitbreidingen.

- CTFd Scoreboard
- CTFd as a Service (CTFd instance per cursus)
- Docker-compose based CTF challenges
- Federated Identity (inloggen met je eigen school account)
- CTF player behavior logging