---
title: Visual Studio Code Extensions & Management
description: 
published: true
date: 2022-01-19T15:12:54.416Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:01:13.882Z
---

# Visual Code Studio Extenstions & Managment

# Settings Sync

Om met het hele team met dezelfde settings te werken in Visual Code Studio is er een handige extension beschikbaar om deze settings te delen. Er is één public gist in GitHub waar de hoofdsettings instaan. Deze kan dan per persoon gedownload worden en ingeladen in Visual Code Studio.

1. Download in Visual Code Studio de extensie **Settings Sync** van Shan Khan.

2. Open het VS Code commando palet met F1 en typ: **Sync: Advanced Options**, selecteer vervolgens: **Sync: Download Settings from Public GIST**.

3. Vul het gist ID in van de public gist in: [https://gist.github.com/JointCTF/8d2b6b30d0d20d6a991988cf21df4d5d](https://gist.github.com/JointCTF/8d2b6b30d0d20d6a991988cf21df4d5d), waarvan het id 8d2b6b30d0d20d6a991988cf21df4d5d is.

4. Druk op F1 en typ in: Sync: Download Settings.

 ![Download Settings](Assets/Download-settings.png)

5. De settings worden nu vanaf de public gist gedownload. In het output scherm is er te zien wat de veranderingen zijn:

 ![Public Gist](Assets/public-gist.png)

## Extensions

> Heb jij nog een goede toevoeging voor in de lijst? Is een exstensie zinloos of te ingewikkeld? Laat het weten, dan verwijderen we deze of schrijven daar documentatie voor.

> :warning: Helaas worden niet alle extensies gesynchroniseerd en GitHub geeft een foutmelding aan, vanwege de grootte. Ook wordt de Profile Switcher configuratie wordt niet meegenomen in de synchronisatie.
>
> Welke extensies zijn overbodig en kunnen weg? Wellicht kunnen we meerdere Gists gebruiken i.p.v. de profile switcher.

## General

- Settings Sync - Sync and share settings with a team

- Profile Switcher - Create profiles for your extenstions and switch between them

- Project Manager - Manage VS Code projects and switch between them

- Code Spell Checker

- Dutch - Code Spell Checker

- Prettier - Code formatter

- ENV - File type supprt

- YAML - Language support

- Remote development - A collection of extenstions for remote development; SSH, WSL or Containers

- vscode-icons - Makes file types recognisable

- Visual Studio IntelliCode - Helps you code by providing suggestions

## Git

- Git Project Manager - Easily switch between multiple Git projects

- GitLens - Advanced features for Git management in VS Code

- Git History - Provides an overview of the history in Git

- GitGraph - Configurable Graph interface and perform Git actions

## Python

- Python - Python language support, utilzing Pylance and Jupyter extensions

- Package Manager - Manage packages installed on PC

- PyPi Assistent - Up-to-date PyPi package information in pip requirements

- AREPL for python - Real-time Python scratchpad

- Code runner - Code snippets with multi-language support

- Pylance - Feature-rich Python language support

## Kubernetes / Docker

- Kubernetes - Manage Kubernetes clusters

- Kubernetes Context - Switch between cluster context

- Kubernetes Templates - Utilize example Kubernetes manifests

## Documentation

- Markdown All in One - Feature-rich markdown enhancer

- Markdown Preview Enhanced - Improved markdown preview

- mdoc - Markdown Documentation Viewer - Full markdown documentation site viewer

- Draw.io integration - Integrates draw.io diagrams in VS Code

## Configuration

- Ansible - Support for the Ansible configuration language

- NGINX Configuration Language Support
