---
title: FAQ for developers
description: 
published: true
date: 2023-03-06T16:02:36.301Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:01:10.006Z
---

Version control overview (plan):
Many repositories have pipelines that build images, or other artefacts. We aim for the following conventions for image tagging:

- `latest` is the latest version that is build on branch main
- `X.Y.Z` is a specific version on branch main, following [semantic versioning](https://semver.org/)
- `branchname` is the head of a feature branch.

We aim for [trunk based development](https://trunkbaseddevelopment.com/).

Pipelines trigger on push to main, and on pull requests where the branch is not main. Also: tagging a commit will trigger a pipeline. The resulting image tags are given above.

---

Obsolete:
- Hoe zit de omgeving in elkaar?
 	- [Services architectuur](../Architectuur-&-Diagrammen/Services-architectuur.html)
  - [Database ERD (Entity Relationship Diagram)](../Documentation/Entity-Relationship-Diagram-(ERD).html)
  - [Deployment Diagram (UML)](../Documentation/Deployment-Diagram-UML.html)

---

- Hoe begin ik met Docker en Kubernetes?
 	- Start met [Docker and Kubernetes for dummies](../Kubernetes/Kubernetes-for-dummies.html)
  - See [Kubernetes Quality of life improvements](../Documentation/Kubernetes-Quality-of-life-improvements.md) to make Kubernetes a bit easier
  - See the [Kubernetes-troubleshooting -tips](../Documentation/Kubernetes-troubleshooting-tips.md) for common problems and solutions

---

- What software is already available?  
 	- The platform is built with State-of-the-Art Cloud-Native technologies and philosophies. We use the following technologies and services:

  		- Applicatie: [CTFd](https://ctfd.io/), [onze plugins en upgrade proces](https://gitlab.com/jointcyberrange.nl/ctfd-jcr/-/blob/main/FAQ.md)
  		- Kubernetes distributie: [RKE](https://rancher.com/docs/rke/latest/en/) & [Kubespray](https://github.com/kubernetes-sigs/kubespray) (wordt uitgefaseerd)
  		- Application: [CTFd](https://ctfd.io/), [our plugins and upgrade process](https://gitlab.com/jointcyberrange.nl/ctfd-jcr/-/blob/main/FAQ.md)
  		- Kubernetes distribution: [RKE](https://rancher.com/docs/rke/latest/en/) & [Kubespray](https://github.com/kubernetes-sigs/kubespray) (being phased out)
    - Ingress NGINX controller
    		- CI/CD & SCM: [GitLab group: Joint Cyber Range]( https://gitlab.com/hu-hc/jcr) & [R2Devops hub](https://r2devops.io)
    		- GitOps: [ArgoCD](https://argocd.dev-jointcyberrange.nl/applications?proj=staging&sync=&health=&namespace=&cluster=&labels=)
    		- Documentatie: [Wiki.js](https://js.wiki/) & README's
    		- Secrets management: [Doppler](https://dashboard.doppler.com/)
    		- IDE: [Visual Studio Code](https://code.visualstudio.com/) or our [portable and pre-configered version](https://gitlab.com/hu-hc/jcr/platform/portable-vscode)

---

- Who will give me access?
 	- GitLab: Peter van Eijk
 	- Microsoft Teams: Peter van Eijk
  - Doppler: Peter van Eijk


---
- Who manages the domain name?
 	- DNSmadeeasy is the DNS provider managed by Peter van Eijk.
 	- We have 2 domains: jointcyberrange.nl & dev-jointcyberrange.nl
  - All DNS records / subdomains can be found in the [DNS overview](../../Documentation/DNS-overview.md) or queried live using DNS enumeration
 	- Peter van Eijk beheert de contactgegevens van mensen die betrekking hebben tot de Joint Cyber Range.

- Who has the contact information?
 	- Peter van Eijk manages the contact information of people related to the Joint Cyber Range. 
