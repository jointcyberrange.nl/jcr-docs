---
title: Surfconext
description: Surfconext management guide
published: true
date: 2022-01-19T15:12:40.918Z
tags: surfconext, openid connect, federated identity
editor: markdown
dateCreated: 2022-01-13T21:22:56.161Z
---

# SURFconext

SURFconext is a federated identity provider, it acts as gateway for IDP's of eductional instidutions. OpenID Connect and SAML are the two protocols supported of which the JCR uses the first.

## Management

The SURFconext Service Providers (SP) can be managed from the [SURFconext Service Provider Dashboard](https://sp.surfconext.nl/). SP's are applications in need of federated identity. You need permissions to access the SP Dashboard by sending an accession request for our [SURFconext team](https://teams.surfconext.nl/teams/40356/members).

## Environments

SURFconext has [two envrionments](https://wiki.surfnet.nl/display/surfconextdev/Environments) you can access for test and production. 

- [Connect to the Test environment](https://wiki.surfnet.nl/display/surfconextdev/Connect+to+the+Test+environment) for testing your OpenID Connect or SAML SP connector with SURFconext.
- [SURFconext production environment in 5 steps](https://wiki.surfnet.nl/display/surfconextdev/Connect+in+5+Steps).

## Requirements

For using the **SURFconext test environment** a view requirements must be met:

- Minimal user information requested by the SP, only essential information. These requests are called claims in OpenID Connect, for SAML these are attributes.
- [Contractual requirements](https://wiki.surfnet.nl/display/surfconextdev/Contractual+part) for your organization.

**SURFconext in production** requires the some extra:

- [SSL Labs test](https://www.ssllabs.com/ssltest/) Rating B or Higher.
- Organizational support for adding a new application / SP. Not any application may be connected to SURFconext.

## Claims

The Joint Cyber Range uses the following OpenID Conncet claims with the motivation for using it.

- **Common name attribute:** Identify users of the platform

- **Email address attribute:** Communication within the platform can be realised using emails.

- **Organization attribute:** Organizational seperation in the platform. One organisation is one school.

- **Affiliation attribute:** Identify users of the platform

- **Preferred language attribute:** Different languages in the platform 

## Service Provider entity config

- **Client id** using a valid URL (recommended to use the (sub)domain+TLD / URL of your app without `https://` prefix)
- **Client Secret** will be generated and only shown ones when the SP entity is published
- **Redirect URL** your app responds to, this is your full app URL with `https://` prefix and suffix of added to the URL `/auth/surfconext/confirm`. When you want to allow more URL's to be accepted, just add them with correct prefix.
- Choose wether to receive the **transient or the persisent** version grants. See for [more information](https://wiki.surfnet.nl/display/surfconextdev/Attribute+best+practice) In short: If you need to identify a returning user, choose persistent, otherwise choose transient.
- **Organization's logo** must be available over a public URL.
- **Name & Description** in Dutch & English
- **Various type of contact information**: administrative, technical and support. It's highly recommended to use functional addresses (i.e. tech@serviceprovider.org) instead of personal addresses.

## Allowed educational institutionsIdP Whitelist

When logging in to an application using SURFconext you can choose which organization to do with using the [WAYF (Were Are You From)](https://wiki.surfnet.nl/display/surfconextdev/SURFconext+WAYF) page. In the [sp.dashboard](https://sp.surfconext.nl/) under IdP whitelist, you can configure for every entity the institutions you allow to be shown in the WAYF page and thus login with.

Edit:
![edit_idp_whitelist.png](Assets/edit_idp_whitelist.png)

List:
![idp_whitelist.png](Assets/idp_whitelist.png)
