---
title: Server overzicht
description: 
published: true
date: 2022-01-14T10:02:02.989Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:00:09.111Z
---

# Server overzicht

Actuele uptime van [sommige services hier](https://status.asm.saas.broadcom.com/67337).

Vraag informatie op m.b.t. nodes van Kubernetes clusters:

```plaintext
kubectl get nodes -o wide
```

Output:

```plaintext
NAME              STATUS   ROLES                      AGE   VERSION   INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
145.101.113.154   Ready    controlplane,etcd,worker   30d   v1.21.5   145.101.113.154   <none>        Ubuntu 20.04.3 LTS   5.4.0-91-generic   docker://19.3.14
145.101.113.155   Ready    controlplane,etcd,worker   30d   v1.21.5   145.101.113.155   <none>        Ubuntu 20.04.3 LTS   5.4.0-91-generic   docker://19.3.14
145.101.113.156   Ready    worker                     30d   v1.21.5   145.101.113.156   <none>        Ubuntu 20.04.3 LTS   5.4.0-91-generic   docker://19.3.14
```

| **Resource** | **Sudo USER/PASSWD** | **Limieten** | **Adressering** | **Status** | **Tags** |
| --- | --- | --- | --- | --- | --- |
| PROD-SURF-1 | ubuntu   <br>/   <br>\-  <br>  <br>(surf keypair) | Core: 2   <br>Memory: 8GB   <br>Disk: 50GB | IPv4: 145.101.113.154   <br>IPv6: 2001:610:188:134:145:101:113:154 | OPERATIONEEL | Development / Staging<br><br>controlplane,etcd,worker |
| PROD-SURF-2 | ubuntu   <br>/   <br>\- | Core: 2   <br>Memory: 8GB   <br>Disk: 50GB | IPv4: 145.101.113.155   <br>IPv6: 2001:610:188:134:145:101:113:155 | OPERATIONEEL | Development / Staging<br><br>controlplane,etcd,worker |
| DEV-SURF-1 | ubuntu   <br>/   <br>\- | Core: 2   <br>Memory: 8GB   <br>Disk: 50GB | IPv4: 145.101.113.156   <br>IPv6: 2001:610:188:134:145:101:113:156 | OPERATIONEEL | Development / Staging<br><br>worker |
| WED-SURF-1 | hu\_student   <br>/   <br>\- | Core: 2   <br>Memory: 4GB   <br>Disk: 20GB | IPv4: 145.101.113.150   <br>IPv6: 2001:610:188:134:145:101:113:150 | OPERATIONEEL | Production<br><br>controlplane,etcd,worker |
| WED-SURF-2 | hu\_student   <br>/   <br>\- | Core: 2   <br>Memory: 4GB   <br>Disk: 20GB | IPv4: 145.101.113.152   <br>IPv6: 2001:610:188:134:145:101:113:152 | OPERATIONEEL | Production<br><br>controlplane,etcd,worker |
| WED-SURF-3 | hu\_student   <br>/   <br>\- | Core: 2   <br>Memory: 4GB   <br>Disk: 20GB | IPv4: 145.101.113.153   <br>IPv6: 2001:610:188:134:145:101:113:153 | OPERATIONEEL | Production<br><br>worker |