---
title: DNS overzicht
description: 
published: true
date: 2022-01-14T08:51:45.631Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:00:05.126Z
---

# DNS overzicht 

Wanneer de huidige records niet volstaan, vraag de gewenste wijziging aan bij de administrator middels een ticket.

Online DNS enumeration for finding public information about subdomains and DNS records using OSINT.

- [Scantrics subdomain-scanner](https://scantrics.io/subdomain-scanner/)
- [DNSdumpster](https://dnsdumpster.com/)

In onderstaande tabel zijn de domein verwijzingen te vinden.

|     | **Status** | **Name** | **Type** | **TTL** | **Value** |
| --- | --- | --- | --- | --- | --- |
| 1   | OPERATIONAL | [hu.jointcyberrange.nl](http://hu.jointcyberrange.nl) | A   | 3600 (default) | `145.101.113.150` |
| 2   | TO DO | [hu.jointcyberrange.nl](http://hu.jointcyberrange.nl) | AAAA | 3600 (default) | n.t.b. |
| 3   | OPERATIONAL | [demo.jointcyberrange.nl](http://hu.jointcyberrange.nl) | A   | 3600 (default) | `145.101.113.150` |
| 4   |     | [dev-jointcyberrange.nl](http://dev-jointcyberrange.nl) | A   | 3600 (default) | `145.101.113.154` |
| 5   | OPERATIONAL | [juice-shop.hu.jointcyberrange.nl](http://juice-shop.hu.jointcyberrange.nl) | A   | 3600 (default) | `145.101.113.154` |
| 6   | TO DO | [juice-shop.hu.jointcyberrange.nl](http://juice-shop.hu.jointcyberrange.nl) | AAAA | 3600 (default) | n.t.b. |
| 7   | OPERATIONAL | \*.prod.hu.jointcyberrange.nl | A   | 3600 (default) | 145.101.113.154 |
| 8   | OPERATIONAL | \*.prod.hu.jointcyberrange.nl | AAAA | 3600 (default) | 2001:610:188:134:145:101:113:154 |
| 9   | TO DO | \*.prod.hu.jointcyberrange.nl | A   | 3600 (default) | 145.101.113.155 |
| 10  | TO DO | \*.prod.hu.jointcyberrange.nl | AAAA | 3600 (default) | 2001:610:188:134:145:101:113:155 |
| 11  | OPERATIONAL | \*.dev.hu.jointcyberrange.nl | A   | 3600 (default) | 145.101.113.156 |
| 12  | OPERATIONAL | \*.dev.hu.jointcyberrange.nl | AAAA | 3600 (default) | 2001:610:188:134:145:101:113:156 |