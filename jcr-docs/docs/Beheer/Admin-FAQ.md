---
title: Admin FAQ
description: Frequently Asked Questions for Administrators
published: true
date: 2022-11-19T13:22:31.776Z
tags: 
editor: markdown
dateCreated: 2022-01-14T11:44:47.247Z
---

# Administrator FAQ

- [Disasters and their recovery](Disasters.md).
- Main [admin interface at Argo CD](https://argocd.dev-jointcyberrange.nl/login). [Argo CD Manuals](https://argo-cd.readthedocs.io/en/stable/).
- Where can I find the internal GitLab repositories of the JCR and what are their purpose?
    - [Getting Started](https://gitlab.com/jointcyberrange/getting-started) with the Joint Cyber Range
    - Platform related
        - [JCR CTFd](https://gitlab.com/jointcyberrange.nl/ctfd-jcr) main app repo
        - [ArgoCD & Kustomize](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize) for GitOps & declarative configuration
        - [K8s cluster deploy](https://gitlab.com/hu-hc/jcr/platform/k8s-cluster-deploy) to bare-metal / SURF VM's
        - [CloudFerro K8s cluster deployment](https://gitlab.com/hu-hc/jcr/platform/cloudferro-k8s-cluster-deployment)  OpenStack Public Cloud
        - [SRE Blameless Post-mortem template](https://gitlab.com/hu-hc/jcr/platform/sre-post-mortem-template) for after incident response
        - [Portable & pre-configred VS Code](https://gitlab.com/hu-hc/jcr/platform/portable-vscode) for use with JCR's technlogy stack
        - [Doppler CLI container images](https://gitlab.com/hu-hc/jcr/platform/doppler-cli-container-images) \*Not in use

- [Wiki.js documentation Git storage](https://gitlab.com/hu-hc/jcr/wiki.js-storage) remote storage & backup for our Docs
- [OpenStack related](https://gitlab.com/hu-hc/jcr/openstack) \*Not in use

## New Tenant
- How to add a new Joint Cyber Range tenant?
    - Follow the instructions in the [GitLab repository](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize#16-adding-ctfd-tenants-in-production).

## Versions
- Where to find and manage different versions of JCR deployments for development purposes?
    - [Versions of the JCR development deployments](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize#19-versions-of-the-jcr-deployments)

## Logging

- Where can I find logs of active workloads?
    - Directory on a k8s node:`/var/log/containers/*.log`
    - Docker Desktop container logs
    - `kubectl logs <PODNAME> -n <NAMESPACE>`
    - [Lens](https://k8slens.dev/) the Kubernetes IDE.
    - [Loki logging usage](../Development/Monitoring-&-Logging/Logging-with-Loki.md#usage).
    - [ArgoCD UI](https://argocd.dev-jointcyberrange.nl/)

## Updating
- How to update the version of CTFd?
    - Read the [CTFd update guide](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/blob/master/FAQ.md#7-how-do-i-keep-everything-up-to-date)
    
---
    
- How to update a CTFd deployment to use the latest image produced by GitLab CI?
	- Images currently use the latest tag, delete the CTFd deployment and it will be applied again with the latest image.

---

- How to update CTFd configuration of an existing deployment?
	- Deleting the CTFd deployment isn't sufficient, data in the database will persist. You'll have to delete all resources related to a CTFd deployment, inluding the Database and persistenvolumes. This can easily be done from within the [ArgoCD UI](https://argocd.dev-jointcyberrange.nl/), find and select your app in ArgoCD and click on the `delete` button. The app will be redeployed shortly.

---

- How can I update the Kubernetes clusters and VM's running with Surf?
	- [Update clusters](https://gitlab.com/hu-hc/jcr/platform/k8s-cluster-deploy#13-maintenance)
  - [Update VM's](https://gitlab.com/hu-hc/jcr/platform/k8s-cluster-deploy#13-maintenance)

---

- How to add an educational institution to allow logging in using SURFconext federated identity?
    - [Add the educational institution to the IdP whitelist](Surfconext.md#allowed-educational-institutionsidp-whitelist).

---

- Where to find & manage secrets regarding the JCR?
    - Find secrets in the [Doppler dashboard](https://dashboard.doppler.com/)
    - See how the [Doppler JCR workspace](Doppler.md) is structured and the [working with Doppler guide](Hosting/Doppler-secrets-management.md)



  - Use the [internal GitLab repo with a Docker image](https://gitlab.com/hu-hc/jcr/platform/s3-compatible-object-storage) that includes S3-API compatible clients s3cmd & Python Boto3.
  - Download and install the [OpenStackClient](https://creodias.eu/-/how-to-install-openstackclient-linux-) and retrieve [EC2 credentials](https://creodias.eu/-/how-to-generate-ec2-credentials-?inheritRedirect=true&redirect=%2Ffaq-s3) (Access Key & Secret Key).
  - You can now use any S3-API compatible client using host `https://s3.waw2-1.cloudferro.com` and the retrieved Access Key & Secret Key.
  
---

- How can I use Civo managed Kubernetes? 
	- Use with [Terraform](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize/-/tree/main/miscellaneous/civo/terraform)
	- Use with [Crossplane](https://gitlab.com/hu-hc/jcr/platform/argocd-kustomize/-/tree/main/miscellaneous/civo/crossplane)