---
title: Doppler
description: 
published: true
date: 2022-01-19T15:12:36.894Z
tags: 
editor: markdown
dateCreated: 2022-01-13T19:49:42.271Z
---

# Doppler

## Projects

Secrets in the [Doppler Workspace: Hu JCR](https://dashboard.doppler.com/workplace/bd1e277a3b116cd73706/projects) reside in projects, each project has by default 3 (dev, stg & prd) environments with configs for the instances under them.

![doppler_projects.png](Assets/doppler_projects.png)

- **jcr_ctfd** secrets for CTFd deployments manageed by ArgoCD and development purposes.
- **jcr_mariadb** secrets for MariaDB deployments related to CTFd deployments mentioned earlier.
- **websites** secrets for various internal websites: Grafana, ArgoCD, Loki
- **jcr-mail** secrets for various JCR mail accounts: Gmail, Outlook, Mail.com
- **surf** secrets related to SURF, either for VM's or Kubernetes cluster

## Service tokens

Note that servicetokens are required for accessing Doppler secrets without manual login. We store these tokens automatically after they're created in the projects `jcr_ctf` & `jcr_mariadb` in the configs `dev_servicetokens` & `prd_servicetokens`.

![doppler_project_jcr_ctfd.png](Assets/doppler_project_jcr_ctfd.png)

## Working with Doppler

Working with Doppler is more [thoroughly explained in another page](Hosting/Doppler-secrets-management.md).
