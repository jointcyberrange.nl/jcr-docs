---
title: Doppler Secrets Management
description: 
published: true
date: 2022-01-19T13:59:09.003Z
tags: 
editor: markdown
dateCreated: 2021-12-21T14:00:13.461Z
---

# Doppler secrets management 

**Official Docs:** [https://docs.doppler.com/docs/start](https://docs.doppler.com/docs/start)

**Kubernetes:** Multiple methods of retrieving secrets are available for Kubernetes, we are currently using the method of: [https://docs.doppler.com/docs/kubernetes-doppler-cli-in-docker](https://docs.doppler.com/docs/kubernetes-doppler-cli-in-docker) in the menu you can see the alternatives.

**Dockefile:** Additional documentation on Dockerfiles with Doppler is available for creating a: [https://docs.doppler.com/docs/dockerfile](https://docs.doppler.com/docs/dockerfile)

**JCR use case:** We use the JCR flavored CTFd image produced by the GitLab pipeline as Base image and wrap the Doppler CLI around it. The same principle is used for the MariaDB image from Docker Hub. Internal docs on this are at: [https://gitlab.com/hu-hc/jcr/platform/doppler-cli-container-images](https://gitlab.com/hu-hc/jcr/platform/doppler-cli-container-images)

**Doppler CLI:** Access Doppler right from your terminal: [https://docs.doppler.com/docs/cli](https://docs.doppler.com/docs/cli)

**Support:** For any open source support regarding Doppler, you can use the: [https://community.doppler.com/](https://community.doppler.com/)

**Limits:** When using Doppler, take in account the [https://docs.doppler.com/docs/platform-limits](https://docs.doppler.com/docs/platform-limits) .

## JCR Doppler projects

The Joint Cyber Range host multiple projects within the Doppler secrets management platform. `jcr_ctfd` is used for all the CTFd instances, such as development environments and tenants. The same applies to `jcr_mariadb`. `Surf` is used to store the SSH keys and kubeconfig for the SURF development en production clusters.

![](Assets/index.png)

Every Doppler project can have multiple environments, such as Development, Staging and Production with multiple branches underneath it.

**Note:** Branched configs inherit secrets from the environment's root config. Each secret in the config can diverge from the root but will then stop inheriting changes.

![](Assets/index2.png)

Note that most secrets also provide some notes on how to use it, where to get it from or what syntax to follow.

**jcr_ctfd**

![](Assets/ind3ex.png)

**jcr\_mariadb**

![](Assets/543453.png)

**surf**

![](Assets/777.png)

## Secrets referencing

The Database URL used by CTFd to connect to MariaDB is made out of various other secrets and information. Doppler has a feature secrets referencing, this can be done within the same config or across projects. The benefit of this is, you don’t have to change the database URL every time the the username or password changes.

The following snippet is an example of how the Database URL in the jcr\_ctfd Doppler project references other secretes from the jcr\_mariadb project.

`mysql+pymysql://${jcr_mariadb.dev.MARIADB_USER}:${jcr_mariadb.dev.MARIADB_PASSWORD}@MARIADB_K8S_SVC.K8S_NAMESPACE.svc.cluster.local/ctfd`

You can retrieve the secret with the Doppler CLI as followed.

`doppler secrets get DATABASE_URL -p jcr_ctfd -c dev_ctfd`

## Managing Doppler with the CLI

Create a Doppler project.

`doppler projects create test_project`

List the environments from the new project.

`doppler environments -p test_project`

Delete the staging environment with the slug `stg`.

`doppler environments delete -p test_project stg`

Create a new config `config1` under the development environment, you have to specify the environment’s slug as prefix when creating a branch config.

`doppler configs create dev_config1 -p test_project -e dev`

Create a service token for `dev_config1`.

`doppler configs tokens create --project test_project --config dev_config1 service_token --plain`

Delete the `test_project`.

`doppler projects delete test_project`

## Uploading secrets with the Doppler CLI

Upload or update a secret from your filesystem as followed.

SSH key:

`cat ~/.ssh/surf | doppler secrets set SSH_PRIVATE_KEY -p surf -c dev_test`

Kubeconfig:

`cat rke/kube_config_dev.yaml | doppler secrets set KUBECONFIG -p surf -c dev`

Look at other methods on the Doppler docs: [https://docs.doppler.com/docs/setting-secrets](https://docs.doppler.com/docs/setting-secrets)

## Downloading secrets as files

### SSH keys

SSH keys can be accessed right from the Doppler CLI. The following command will save the SSH private key from the SURF development cluster as plaintext to a specified location. With `-p` you specify the Doppler project and with `-c` you specify a config withing a Doppler project.

`doppler secrets get SSH_PRIVATE_KEY --plain > ./surf-ssh -p surf -c dev`

### Kubeconfig

The same principle as SSH keys can be used for a Kubernetes kubeconfig.

`doppler secrets get KUBECONFIG --plain > ./surf-dev-kubeconfig -p surf -c dev`

### To-Do

[https://docs.doppler.com/docs/kubernetes-doppler-cli-in-docker#kubernetes-secret](https://docs.doppler.com/docs/kubernetes-doppler-cli-in-docker#kubernetes-secret): Using Kubernetes secret for Serice token

[https://docs.doppler.com/docs/enclave-gitlab](https://docs.doppler.com/docs/enclave-gitlab): Integration with CI/CD pipelines

[https://docs.doppler.com/docs/kubernetes-operator](https://docs.doppler.com/docs/kubernetes-operator): More scalable and sustainable in production

[https://docs.doppler.com/docs/docker-high-availability](https://docs.doppler.com/docs/docker-high-availability): For Doppler outages

[https://docs.doppler.com/docs/editors-vs-code](https://docs.doppler.com/docs/editors-vs-code) : Integration with the Editor for env-files

Effiecient Doppler workflow when creating tenants (e.g. with Doppler CLI)

### Extra

Doppler also offers a CLI tool for which the guide can be found here: [https://docs.doppler.com/docs/cli](https://docs.doppler.com/docs/cli)

Hogeschool Utrecht Students and personnel\`have access to a Azure subscription. If desired, it is very easy to setup an integration and sync secrets with an:[https://docs.doppler.com/docs/azure-key-vault](https://docs.doppler.com/docs/azure-key-vault). If doppler proves insufficient or too expensive, Azure Key Vault may be the best alternative.